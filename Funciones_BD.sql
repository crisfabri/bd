	
------------------------------------------------------------
--FUNCION CREAR USUARIO

CREATE OR REPLACE FUNCTION FX_SIS_INS_USUARIO(
	p_nombres varchar, 
	p_apellido_m varchar,
	p_apellido_p varchar,
	p_correo varchar,
	p_clave varchar,
	p_cel int,
	p_telf int)
 RETURNS TABLE(lresultado boolean, cmensaje character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	lnNrDoc INT;
	lncodigo INT;
	lnperfil INT;
BEGIN
	SELECT A.ncodigo INTO lnNrDoc FROM sis_m_usuarios AS A WHERE A.ccorreo = p_correo;
	IF lnNrDoc is not NULL THEn
		lresultado = FALSE;
		cmensaje = 'CORREO YA REGISTRADO';
	ELSE 
		INSERT INTO SIS_M_USUARIOS(cnombres,capellido_p,capellido_m,ccorreo,cclave,ncel,ntelf,cestado,tfecha_registro) VALUES (p_nombres,p_apellido_p,p_apellido_m,p_correo,md5(p_clave),p_cel,p_telf,'A',current_date);
		
		--CREAMOS PERFILE CLIENTE
		SELECT A.ncodigo into lncodigo from sis_m_usuarios AS A where A.ccorreo = p_correo;
		SELECT ncodigo INTO lnperfil FROM SIS_T_PERFILES WHERE cNombre = 'CLIENTE';
		INSERT INTO SIS_P_PERFILES(ncodigo_perfil,ncodigo_u,cestado) values(lnperfil,lncodigo,'A');
		lresultado = TRUE;
		cmensaje = 'USUARIO REGISTRADO CORRECTAMENTE';
	END IF;
RETURN NEXT;
RETURN;
END 
$BODY$;


--------------------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_sis_ins_log(
	p_ncodusu integer,
	p_ncodopc integer,
	p_cglosa character varying)
    RETURNS TABLE(lresultado boolean, cmensaje character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
BEGIN
	INSERT INTO SIS_T_LOG (nCodigo_Usuario, nCodigo_Opcion, cGlosa)
		VALUES (p_nCodUsu, p_nCodOpc, UPPER(p_cGlosa));
	lResultado = TRUE;
	RETURN NEXT;
	RETURN;
END
$BODY$;

------------------------------------------------------------------------------------------------------------------------------------
--LOGIN 

CREATE OR REPLACE FUNCTION public.fx_login(IN p_correo character varying,IN p_clave character varying)
    RETURNS TABLE(lresultado boolean, cmensaje character varying, cperfil character varying)
    LANGUAGE 'plpgsql'
    VOLATILE
    PARALLEL UNSAFE
    COST 100    ROWS 1000 
    
AS $BODY$DECLARE
	lcEstado CHAR(1); 
	lnCodUsu INT;
	lnopcion INT;
BEGIN
	SELECT ncodigo into lnopcion FROM sis_t_opciones WHERE cnombre = 'LOGIN';
	SELECT cestado, cNombres, nCodigo INTO lcEstado, cMensaje, lnCodUsu
		FROM SIS_M_USUARIOS
		WHERE UPPER(ccorreo) = UPPER(p_correo) AND cClave = MD5(p_clave);
	IF cMensaje IS NULL THEN
		lResultado = FALSE;
		cMensaje = 'Correo o contraseña invalidos';
	ELSE	
		IF lcEstado = 'A' THEN
			lResultado = TRUE;
			SELECT A.lResultado INTO lResultado FROM fx_SIS_ins_Log(lnCodUsu,lnopcion, 'INGRESO') A;
			
			SELECT cNombre INTO cPerfil 
			FROM SIS_T_PERFILES A 
			INNER JOIN SIS_P_PERFILES B ON A.nCodigo = B.nCodigo_Perfil 
			WHERE nCodigo_U = lnCodUsu;
			
			cPerfil = COALESCE(cPerfil, '');
			lResultado = TRUE;
			cMensaje = 'Bienvenido';
		ELSE
			SELECT A.lResultado INTO lResultado FROM fx_SIS_Ins_Log(lnCodUsu,lnopcion, '[ERROR][USUARIO INACTIVO]') A;
			lResultado = FALSE;
			cMensaje = 'Cuenta NO se encuentra activa';
		END IF;
	END IF;
	RETURN NEXT;
	RETURN;
END
$BODY$;
---------------------------------------------------------------------------------------------------------------------------------
--FUNCION INGRESAR PEDIDO


CREATE OR REPLACE FUNCTION public.fx_sis_ins_pedido(IN p_ccorreo character varying,IN p_ctippag character,IN p_ntotal double precision,IN p_ndireccion integer)
    RETURNS TABLE(lresultado boolean, cmensaje character varying, ncodped integer)
    LANGUAGE 'plpgsql'
    VOLATILE
    PARALLEL UNSAFE
    COST 100    ROWS 1000 
    
AS $BODY$DECLARE
	lnCodigo INT;
	ctippag char;
	lncodCli int;
	nestado int;
BEGIN	
	IF p_ndireccion = -1 THEN
		SELECT ncodigo into p_ndireccion FROM sis_t_direcciones WHERE cdireccion = 'EN LOCAL';
	END IF;
	SELECT ncodigo into lncodCli FROM sis_m_usuarios WHERE UPPER(ccorreo) = UPPER(p_ccorreo);
	
	INSERT INTO SIS_M_PEDIDOS(ncliente,ctippag,ntotal,ndireccion,cestado,tfecha_creacion,thora) 
	VALUES (lncodCli,p_ctippag,p_ntotal,p_ndireccion,'A',current_date,now());
	
	SELECT MAX(ncodigo) INTO lnCodigo FROM SIS_M_PEDIDOS;
	
	SELECT A.ncodigo into nestado from SIS_T_ESTADOSPEDIDO AS A WHERE A.cnombre = 'SOLICITADO';
	
	INSERT INTO SIS_P_ESTADO_PEDIDO(npedido,nestado,tfecha_creacion,nusuario_crea,thora)
	VALUES(lnCodigo,nestado,current_date,1,now());
	--FALTA REGISTRAR DOCUMENTO DE PEDIDO
	ncodPed = lnCodigo;
	lresultado = TRUE;
	cmensaje = 'PEDIDO REGISTRADO';
	
RETURN NEXT;
RETURN;
END;
$BODY$;


------------------------------------------------------------------------------------------------------------------------------------
--FUNCION INSERTAR DETALLE PEDIDO

CREATE OR REPLACE FUNCTION FX_SIS_INS_DETPEDIDO(
p_npedido int,
p_nproducto int,
p_ncantidad int,
p_npreciounitario float)
RETURNS TABLE(lresultado boolean, cmensaje character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	lnUsuario int;
	lnstock int;
BEGIN	
	lnUsuario = 1;
	--INSERTAMOS DETALLE DE PEDIDO
	INSERT INTO SIS_D_PEDIDOS(npedido,nproducto,ncantidad,npreciounitario,ntotal,cestado,tfecha_creacion) VALUES (p_npedido,p_nproducto,p_ncantidad,p_npreciounitario,p_ncantidad*p_npreciounitario,'A',current_date);
	
	--ACTUALIZAMOS STOCK DE PRODUCTO PEDIDO
	UPDATE SIS_M_PRODUCTOS SET nstock = nstock - p_ncantidad WHERE ncodigo = p_nproducto;
	--SI ES QUE PRODUCTO SE AGOTA
	SELECT A.nstock INTO lnstock FROM SIS_M_PRODUCTOS AS A WHERE A.ncodigo = p_nproducto;
	IF lnstock = 0 THEN
		UPDATE SIS_M_PRODUCTOS SET cestado = 'I' WHERE ncodigo = p_nproducto;
	END IF;
	--INSERTAMOS MOVIMIENTO REALIZADO
	INSERT INTO SIS_T_MOVIMIENTOS(nproducto,ncantidad,cestado,nusuario_crea) VALUES (p_nproducto,-1*p_ncantidad,'A',lnUsuario);
	lresultado = TRUE;
	cmensaje = 'PEDIDO REGISTRADO';
	
RETURN NEXT;
RETURN;
END 
$BODY$;

-----------------------------------------------------------------------------------------------------------------------
--ACTUALIZAMOS ESTADO PEDIDOS

CREATE OR REPLACE FUNCTION public.fx_sis_act_pedido(IN p_npedido integer,IN p_cestado integer)
    RETURNS TABLE(lresultado boolean, cmensaje character varying)
    LANGUAGE 'plpgsql'
    VOLATILE
    PARALLEL UNSAFE
    COST 100    ROWS 1000 
    
AS $BODY$DECLARE
BEGIN
	 
	UPDATE SIS_P_ESTADO_PEDIDO SET nestado = p_cestado, tfecha_creacion = current_date, nusuario_crea = 1, thora = now()
	where npedido = p_npedido;
	--ACTUALIZAMOS ESTADO DE DETALLE DE PEDIDO
	--UPDATE SIS_D_PEDIDO AS A SET A.estado = p_cestado WHERE A.npedido = p_npedido;
	lresultado = true;
	cmensaje = 'Estado del pedido actualizado';
	RETURN NEXT;
RETURN;
END 
$BODY$;

---------------------------------------------------------------------------------------------------------------
--AGREGAR PRODUCTO

CREATE OR REPLACE FUNCTION public.fx_sis_ins_producto(
	p_nombre character varying,
	p_descripcion character varying,
	p_precio double precision,
	p_img text,
	p_nstock integer,
	p_ncategoria integer)
    RETURNS TABLE(lresultado boolean, cmensaje character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	lncodigo INT;
BEGIN

	INSERT INTO SIS_M_PRODUCTOS(cnombre,cdescripcion,nprecio,img,cestado,ncategoria,nstock) VALUES (p_nombre,p_descripcion,p_precio,p_img,'A',p_ncategoria,p_nstock);
	SELECT MAX(ncodigo) INTO lnCodigo FROM SIS_M_PRODUCTOS;
	INSERT INTO SIS_T_MOVIMIENTOS(nproducto,ncantidad,cestado,nusuario_crea) VALUES (lnCodigo,p_nstock,'A',1);
	lresultado = TRUE;
	cmensaje = 'PRODUCTO AGREGADO CORRECTAMENTE';
	RETURN NEXT;
RETURN;
END 
$BODY$;

------------------------------------------------------------------------------------------------------------------
--AUMENTAR STOCK PRODUCTO
	CREATE OR REPLACE FUNCTION FX_SIS_ACT_STOCK(
	p_nproducto varchar,
	p_ncantidad int)
	RETURNS TABLE(lresultado boolean, cmensaje character varying) 
		LANGUAGE 'plpgsql'

		COST 100
		VOLATILE 
		ROWS 1000

	AS $BODY$
	DECLARE
		lnUsuario int;
	BEGIN
		--USUARIO REGISTRA--FALTA FX_USUARIO()---
		lnUsuario = 1;
		--ACTUALIZAMOS STOCK DE PRODUCTO
		UPDATE SIS_M_PRODUCTOS SET nstock = nstock + p_ncantidad and cestado = 'A' WHERE cnombre = p_nproducto;
		--REGISTRAMOS MOVIMIENTO
		INSERT INTO SIS_T_MOVIMIENTOS(nproducto,ncantidad,cestado,nusuario_crea) VALUES (p_nproducto,p_ncantidad,'A',lnUsuario);
		lresultado = TRUE;
		cmensaje = 'STOCK ACTUALIZADO CORRECTAMENTE';
RETURN NEXT;
RETURN;
END; 
$BODY$;
-----------------------------------------------------------------------------------------------------
--SELECCIONAMOS PEDIDOS POR FECHA
CREATE OR REPLACE FUNCTION public.fx_sis_sel_pedidos(
	p_dfecini date,
	p_dfecfin date,
	p_cnombrecli character varying,
	p_nestado integer)
    RETURNS TABLE(ncodigo integer, ncodcli integer, cnomcli character varying, cdireccion character varying, cestado character varying, nestado character varying, ntotal double precision, tcreacion date,thora TIME,tippag char,correo varchar,ncel int) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	lnUsuario int;
	XPED RECORD;
BEGIN
	FOR XPED IN SELECT A.ctippag,A.thora,C.nestado,A.ncodigo,A.ncliente,A.ntotal,A.tfecha_creacion,A.cestado,A.ndireccion,B.cnombres ||' '|| B.capellido_p ||' '|| B.capellido_m as cnombrec,B.ncodigo as codCLI,B.ccorreo,B.ncel
	FROM sis_m_pedidos as A
	INNER JOIN SIS_P_ESTADO_PEDIDO AS C ON C.npedido = A.ncodigo
	INNER JOIN sis_m_usuarios AS B on B.ncodigo = A.ncliente 
	where
	(CASE 
			WHEN (p_dfecini is not NULL AND p_dfecfin is not NULL )
			THEN A.tfecha_creacion::DATE between p_dfecini and p_dfecfin
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (p_cnombreCli <> '')
			THEN B.cnombres ||''|| B.capellido_p ||''|| B.capellido_m ILIKE '%'||p_cnombreCli||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (p_nestado <> -1)
			THEN C.nestado = p_nestado
			ELSE TRUE
	END)
	LOOP
		select C.cdireccion into cdireccion from sis_t_direcciones AS C where C.ncodigo = XPED.ndireccion;
		SELECT A.cnombre, A.ncodigo into cestado, nestado from sis_T_estadospedido AS A where A.ncodigo = XPED.nestado;
		tippag = XPED.ctippag;
		thora = XPED.thora;
		ncodigo = XPED.ncodigo;
		ntotal = XPED.ntotal;
		cnomcli = XPED.cnombrec;
		ncodcli = XPED.codCLI;
		tcreacion = XPED.tfecha_creacion;
		correo = XPED.ccorreo;
		ncel = XPED.ncel;
		RETURN NEXT;
	END LOOP;
RETURN;
END 
$BODY$;
--select * from sis_m_PEDIDOS
--select ncodigo,ncodcli,cnomcli,cestado,nestado,correo,ncel ntotal,tcreacion,cdireccion,thora,tippag FROM FX_SIS_SEL_PEDIDOS(null,null,'sss',-1) order by tcreacion desc
----------------------------------------------------------------------------------------------------------
--SELECCIONAMOS PEDIDOS DE CLIENTE
CREATE OR REPLACE FUNCTION FX_SIS_SEL_PEDIDOS_CLIENTE(p_ccorreo varchar, p_fecIni DATE, p_fecFin DATE)
RETURNS TABLE(ncodPed int,ntotal int,fechaP DATE,thora TIME,cestado varchar,cdireccion varchar) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
XPED RECORD;
BEGIN
	FOR XPED IN SELECT A.ncodigo,A.ntotal,A.tfecha_creacion,A.thora,A.ndireccion,A.ncliente,B.ccorreo,C.nestado
	FROM sis_m_pedidos as A 
	INNER JOIN SIS_M_USUARIOS AS B ON A.ncliente = B.ncodigo
	INNER JOIN SIS_P_ESTADO_PEDIDO AS C ON C.npedido = A.ncodigo 
	where  UPPER(B.ccorreo) = UPPER(p_ccorreo) 
	AND (CASE 
			WHEN (p_fecIni is not NULL)
			THEN A.tfecha_creacion  >= p_fecIni
			ELSE TRUE
		END)
		AND
		(CASE
			WHEN (p_fecFin is not NULL)
			THEN A.tfecha_creacion  <= p_fecFin
			ELSE TRUE
		END)
	LOOP
		SELECT cnombre into cestado FROM sis_t_estadospedido WHERE ncodigo = XPED.nestado;
		ncodPed = XPED.ncodigo;
		ntotal =  XPED.ntotal;
		fechaP = XPED.tfecha_creacion;
		thora = XPED.thora;
		SELECT A.cdireccion into cdireccion FROM sis_t_direcciones AS A WHERE A.ncodigo = XPED.ndireccion;
	RETURN NEXT;
	END LOOP;
RETURN;
END 
$BODY$;
--select ncodped,cestado,ntotal,fechap,cdireccion,thora FROM FX_SIS_SEL_PEDIDOS_CLIENTE('ocolapintoc@gmail.com',null,null)
----------------------------------------------------------------------------------------------------------------------
--SELECCIONAMOS DETALLE DE PEDIDO CON PRODUCTOS
CREATE OR REPLACE FUNCTION public.fx_sis_sel_detpedido(
	p_ncodped integer)
    RETURNS TABLE(ncodpro integer, cnompro character varying, ncantidad integer, npreciounitario integer, ntotal integer, tfecha date, thora time without time zone, nestado integer) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
XPED RECORD;
BEGIN
	FOR XPED IN SELECT A.nproducto,A.ncantidad,A.npreciounitario,A.ntotal
	FROM sis_d_pedidos as A 
	where A.npedido = p_ncodPed
	LOOP
		SELECT B.nestado,B.tfecha_creacion,B.thora into nestado,tfecha,thora FROM sis_p_estado_pedido AS B WHERE B.npedido = p_ncodPed;
		SELECT ncodigo,cnombre into ncodPro, cnomPro FROM sis_m_productos WHERE ncodigo = XPED.nproducto;
		ncantidad =  XPED.ncantidad;
		npreciounitario = XPED.npreciounitario;
		ntotal = XPED.ntotal;
	RETURN NEXT;
	END LOOP;
RETURN;
END 
$BODY$;


-----------------------------------------------------------------------------------------------------------------------------
--Seleccion Producto


CREATE OR REPLACE FUNCTION public.fx_sis_sel_producto(
	p_ncodpro integer)
    RETURNS TABLE(ncodigo integer, cnompro character varying, cdescripcion character varying, nprecio integer, img text, cestado character, ccategoria character varying, ncategoria integer, nstock integer) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$DECLARE
BEGIN
	SELECT A.ncodigo, A.cnombre,A.cdescripcion,A.nprecio,A.img,A.cestado, A.ncategoria, 
	B.cnombre AS ccategoria,A.nstock
	into ncodigo, cnomPro, cdescripcion,nprecio,img,cestado, ncategoria, ccategoria,nstock
	from SIS_M_PRODUCTOS AS A
	INNER JOIN sis_t_categoria AS B ON B.ncodigo = A.ncategoria 
	WHERE A.ncodigo = p_ncodPro;
RETURN NEXT;
RETURN;
END 
$BODY$;
---------------------------------------------------------------------------------------
--BUSQUEDA PRODUCTO
CREATE OR REPLACE FUNCTION FX_SIS_SEL_PRODUCTO(p_cnomPro varchar)
RETURNS TABLE(cnomPro varchar,cdescripcion varchar,nprecio int,img bytea,cestado char,nstock INT)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
BEGIN
	IF p_cnomPro != '' THEN 
		SELECT A.cnombre,A.cdescripcion,A.nprecio,A.img,A.cestado,A.nstock into cnomPro,cdescripcion,nprecio,img,cestado,nstock from SIS_M_PRODUCTOS AS A WHERE (A.cNombre ILIKE '%'||p_cnomPro||'%') AND A.cestado = 'A';
	ELSE 
		SELECT A.cnombre,A.cdescripcion,A.nprecio,A.img,A.cestado,A.nstock into cnomPro,cdescripcion,nprecio,img,cestado,nstock from SIS_M_PRODUCTOS as A WHERE A.cestado = 'A';
	END IF;
RETURN NEXT;
RETURN;
END 
$BODY$;

--------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION FX_SIS_SEL_PRODUCTO_CATEGORIA(p_ncategoria int)
RETURNS TABLE(cnomPro varchar,cdescripcion varchar,nprecio int,img bytea,cestado char,nstock INT)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
BEGIN
	IF p_ncategoria != -1 THEN 
		SELECT A.cnombre,A.cdescripcion,A.nprecio,A.img,A.cestado,A.nstock into cnomPro,cdescripcion,nprecio,img,cestado,nstock from SIS_M_PRODUCTOS as A WHERE A.ncategoria = p_ncategoria;
	ELSE
		SELECT A.cnombre,A.cdescripcion,A.nprecio,A.img,A.cestado,A.nstock into cnomPro,cdescripcion,nprecio,img,cestado,nstock from SIS_M_PRODUCTOS as A;
	END IF;	
RETURN NEXT;
RETURN;
END 
$BODY$;

------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_sel_producto_cliente(
	)
    RETURNS TABLE(cnompro character varying, cdescripcion character varying, nprecio double precision, img character varying, cestado character, nstock integer, ncodpro integer, ncategoria integer, ccategoria character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	XPRO RECORD;
BEGIN
	FOR XPRO IN SELECT A.cnombre,A.cdescripcion,A.nprecio,A.img,A.cestado,A.nstock, A.ncodigo, A.ncategoria
	from SIS_M_PRODUCTOS AS A WHERE A.cestado = 'A'
	LOOP
	cnomPro= XPRO.cnombre;
	cdescripcion= XPRO.cdescripcion;
	nprecio= XPRO.nprecio;
	img= XPRO.img;
	cestado= XPRO.cestado;
	nstock= XPRO.nstock;
	ncodpro = XPRO.ncodigo;
	SELECT ncodigo, cnombre INTO ncategoria, ccategoria FROM sis_t_categoria AS B WHERE B.ncodigo = XPRO.ncategoria;
	RETURN NEXT;	
	END LOOP;

RETURN;
END 
$BODY$;

-----------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_sel_producto_Admin(
	)
    RETURNS TABLE(cnompro character varying, cdescripcion character varying, nprecio double precision, img character varying, cestado character, nstock integer, ncodpro integer, ncategoria integer, ccategoria character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	XPRO RECORD;
BEGIN
	FOR XPRO IN SELECT A.cnombre,A.cdescripcion,A.nprecio,A.img,A.cestado,A.nstock, A.ncodigo, A.ncategoria
	from SIS_M_PRODUCTOS AS A
	LOOP
	cnomPro= XPRO.cnombre;
	cdescripcion= XPRO.cdescripcion;
	nprecio= XPRO.nprecio;
	img= XPRO.img;
	cestado= XPRO.cestado;
	nstock= XPRO.nstock;
	ncodpro = XPRO.ncodigo;
	SELECT ncodigo, cnombre INTO ncategoria, ccategoria FROM sis_t_categoria AS B WHERE B.ncodigo = XPRO.ncategoria;
	RETURN NEXT;	
	END LOOP;

RETURN;
END 
$BODY$;

--------------------------------------------------------------------------------------------------
--SELECCIONAR CLIENTE
CREATE OR REPLACE FUNCTION public.fx_sis_sel_cliente(
	p_ccorreo character varying)
    RETURNS TABLE(ncodcli integer, cnomcli character varying, capellcli_p character varying, capellcli_m character varying, ccorreo character varying, ncel integer,ntelf integer, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$DECLARE
BEGIN
	SELECT A.ncodigo, A.cnombres,A.capellido_p, A.capellido_m,A.ccorreo,A.ncel,A.ntelf,A.cestado INTO ncodCli, cnomCli,capellCli_p, capellCli_m,ccorreo,ncel,ntelf,cestado 
	FROM sis_m_usuarios AS A 
	WHERE UPPER(A.ccorreo) = UPPER(p_ccorreo);
RETURN NEXT;
RETURN;
END 
$BODY$;

----------------------------------------------------------------------------------------------------------------------------------------
--AÑADIR DIRECCION
CREATE OR REPLACE FUNCTION FX_SIS_INS_DIRECCION(p_ccorreo varchar ,p_cdireccion varchar,p_ndistrito int,p_creferencia varchar)
RETURNS TABLE(lresultado boolean, cmensaje character varying)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	lncodDir int;
	lncodUsu int;
BEGIN
	SELECT ncodigo into lncodUsu FROM SIS_M_USUARIOS WHERE UPPER(ccorreo) = UPPER(p_ccorreo);
	INSERT INTO SIS_T_DIRECCIONES(cdireccion,ndistrito,creferencia) values (p_cdireccion,p_ndistrito,p_creferencia);
	
	SELECT ncodigo INTO lncodDir FROM SIS_T_DIRECCIONES WHERE cdireccion = p_cdireccion and ndistrito = p_ndistrito and creferencia = p_creferencia;
	INSERT INTO SIS_P_DIRECCION_CLIENTE(ncoddir,ncodusu) values(lncodDir,lncodUsu);
	lresultado = TRUE;
	cmensaje = 'DIRECCION AGREGADA CORRECTAMENTE';
	RETURN NEXT;
RETURN;
END 
$BODY$;

--------------------------------------------------------------------------------------------------------------------------------------
--DIRECCIONES DE CLIENTE
CREATE OR REPLACE FUNCTION public.fx_sis_sel_direcciones(
	p_ccorreo varchar)
    RETURNS TABLE(p_ndireccion int, p_cdireccion character varying, p_ndistrito integer, p_cdistrito character varying, p_creferencia character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	lncodDir int;
	lncodUsu INT;
	XDIR RECORD;
BEGIN
	SELECT ncodigo into lncodUsu FROM sis_m_usuarios WHERE UPPER(ccorreo) = UPPER(p_ccorreo);
	FOR XDIR IN SELECT A.ncodigo,A.cdireccion,A.ndistrito,A.creferencia FROM SIS_T_DIRECCIONES AS A
	INNER JOIN SIS_P_DIRECCION_CLIENTE AS B ON A.ncodigo = B.ncoddir
	WHERE B.ncodusu = lncodUsu
	LOOP
		p_cdireccion = XDIR.cdireccion;
		p_ndistrito = XDIR.ndistrito;
		p_creferencia = XDIR.creferencia;
		p_ndireccion = XDIR.ncodigo;
		SELECT cnombre INTO p_cdistrito FROM sis_t_distritos WHERE ncodigo = XDIR.ndistrito;
	RETURN NEXT;
	END LOOP;
RETURN;
END;
$BODY$;
---------------------------------------------------------------------------------------------------------------------------------------------------
--BUSQUEDA PRODUCTOS POR CATEGORIA
CREATE OR REPLACE FUNCTION FX_SIS_SEL_PRODUCTOS_CATEGORIA(p_ncategoria int)
RETURNS TABLE(nombrep varchar,descripcion varchar,precio varchar,img bytea,ccategoria varchar,p_nstock int)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	lncodDir int;
BEGIN
	SELECT A.cnombre,A.cdescripcion,A.nprecio,B.cnombre,A.img INTO nombrep,descripcion,precio,ccategoria,img FROM SIS_M_PRODUCTOS AS A
	INNER JOIN SIS_T_CATEGORIA AS B ON A.ncategoria = B.ncodigo
	WHERE B.ncodigo = p_ncategoria AND A.cestado = 'A';
RETURN NEXT;
RETURN;
END;
$BODY$;
-----------------------------------------------------------------------------------------------------------------------------------------------
--SELECT CATEGORIAS
CREATE OR REPLACE FUNCTION FX_SIS_SEL_CATEGORIAS()
RETURNS TABLE(cnombreCat varchar, ncodCat int)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	XCAT RECORD;
BEGIN
	FOR XCAT IN Select ncodigo,cnombre FROM sis_t_categoria
	LOOP
		ncodCat= XCAT.ncodigo;
		cnombreCat = XCAT.cnombre;
	RETURN NEXT;	
	END LOOP;
RETURN;
END;
$BODY$;

----------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_ins_productosinimg(IN p_nombre character varying,IN p_descripcion character varying,IN p_precio double precision,IN p_nstock integer DEFAULT NULL::integer,IN p_ncategoria integer DEFAULT  NULL::integer)
    RETURNS TABLE(lresultado boolean, cmensaje character varying)
    LANGUAGE 'plpgsql'
    VOLATILE
    PARALLEL UNSAFE
    COST 100    ROWS 1000 
    
AS $BODY$DECLARE
	lncodigo INT;
BEGIN

	INSERT INTO SIS_M_PRODUCTOS(cnombre,cdescripcion,nprecio,cestado,ncategoria,nstock) VALUES (p_nombre,p_descripcion,p_precio,'A',p_ncategoria,p_nstock);
	SELECT MAX(ncodigo) INTO lnCodigo FROM SIS_M_PRODUCTOS;
	INSERT INTO SIS_T_MOVIMIENTOS(nproducto,ncantidad,cestado,nusuario_crea) VALUES (lnCodigo,p_nstock,'A',1);
	lresultado = TRUE;
	cmensaje = 'PRODUCTO AGREGADO CORRECTAMENTE';
	RETURN NEXT;
RETURN;
END 
$BODY$;

-----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.FX_SIS_SEL_PRODUCTO_CATEGORIA_NOMBRE(
	p_ncategoria integer, p_cnomPro varchar,p_cestado char)
    RETURNS TABLE(ncodpro integer, cnompro character varying, cdescripcion character varying, nprecio integer,
				  img text, cestado character, nstock integer,
				  ncategoria integer, ccategoria varchar) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
 XPRO RECORD;
BEGIN
	FOR XPRO IN SELECT A.cnombre,A.cdescripcion,A.nprecio,A.img,A.cestado,A.nstock, A.ncodigo, A.ncategoria
	from SIS_M_PRODUCTOS as A
	WHERE 
		(CASE 
			WHEN (p_ncategoria <> -1)
			THEN A.ncategoria = p_ncategoria
			ELSE TRUE
		END)
		AND
		(CASE 
			WHEN (p_cnomPro <> '')
			THEN A.cnombre ILIKE '%'||p_cnomPro||'%'
			ELSE TRUE
		END)
		AND
		(CASE 
			WHEN (p_cestado <> 'N')
			THEN A.cestado = p_cestado
			ELSE TRUE
		END)	
	LOOP
	ncodpro = XPRO.ncodigo;
	cnomPro= XPRO.cnombre;
	cdescripcion= XPRO.cdescripcion;
	nprecio= XPRO.nprecio;
	img = XPRO.img;
	cestado= XPRO.cestado;
	nstock= XPRO.nstock;
	SELECT ncodigo, cnombre INTO ncategoria, ccategoria FROM sis_t_categoria AS B WHERE B.ncodigo = XPRO.ncategoria;

	RETURN NEXT;	
	END LOOP;
RETURN;
END 
$BODY$;
----------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_act_producto(
	p_ncodpro integer,
	p_cnombre character varying,
	p_cdescripcion character varying,
	p_nprecio double precision,
	p_img text,
	p_cestado character DEFAULT 'A'::bpchar,
	p_nstock integer DEFAULT 0,
	p_ncategoria integer DEFAULT 40)
    RETURNS TABLE(lresultado character varying, cmensaje character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	lncodigo INT;
BEGIN
	UPDATE SIS_M_PRODUCTOS SET cnombre = p_cnombre, cdescripcion = p_cdescripcion ,nprecio = p_nprecio , img = p_img , cestado = p_cestado , ncategoria = p_ncategoria , nstock = p_nstock
	WHERE ncodigo = p_ncodPro;
	lresultado = TRUE;
	cmensaje = 'PRODUCTO ACTUALIZADO';
RETURN NEXT;
END;
$BODY$;

---------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION FX_SIS_ACT_PRODUCTO_SINIMG(
p_ncodPro int,
p_cnombre varchar,
p_cdescripcion varchar,
p_nprecio float,
p_cestado varchar,
p_nstock int, 
p_ncategoria int)
RETURNS TABLE(lresultado varchar, cmensaje varchar)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	lncodigo INT;
BEGIN
	UPDATE SIS_M_PRODUCTOS SET cnombre = p_cnombre, cdescripcion = p_cdescripcion ,nprecio = p_nprecio , cestado = p_cestado , ncategoria = p_ncategoria , nstock = p_nstock
	WHERE ncodigo = p_ncodPro;
	lresultado = TRUE;
	cmensaje = 'PRODUCTO ACTUALIZADO';
RETURN NEXT;
END;
$BODY$;
-------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION FX_SIS_SEL_DISTRITOS()
RETURNS TABLE(ncodDist varchar, cnomDist varchar)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	XDIST RECORD;
BEGIN
	FOR XDIST IN SELECT ncodigo,cnombre FROM SIS_T_DISTRITOS
	LOOP
		ncodDist = XDIST.ncodigo;
		cnomDist = XDIST.cnombre;
	RETURN NEXT;
	END LOOP;
RETURN;
END;
$BODY$;

-----------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION FX_SIS_ACT_CLIENTE(
p_ccorreoCli varchar,
p_cnombre varchar,
p_capellido_p varchar,
p_capellido_m varchar,
p_nrcel int,
p_nrtelf int default null)
RETURNS TABLE(lresultado boolean, cmensaje varchar)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	XDIST RECORD;
	lncliente INT;
BEGIN
	SELECT ncodigo INTO lncliente FROM SIS_M_USUARIOS WHERE UPPER(ccorreo) = UPPER(p_ccorreoCli);
	UPDATE SIS_M_USUARIOS SET cnombres=p_cnombre , capellido_p=p_capellido_p , capellido_m = p_capellido_m , ncel = p_nrcel , ntelf = p_nrtelf
	WHERE ncodigo = lncliente;
	lresultado = TRUE;
	cmensaje = 'PERFIL ACTUALIZADO';
RETURN NEXT;
END;
$BODY$;
-------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION FX_SIS_SEL_ESTADOS()
RETURNS TABLE(ncodEstados varchar, nnomEstados varchar)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	XEST RECORD;
BEGIN
	FOR XEST IN SELECT ncodigo,cnombre FROM SIS_T_ESTADOSPEDIDO
	LOOP
		ncodEstados = XEST.ncodigo;
		nnomEstados = XEST.cnombre;
	RETURN NEXT;
	END LOOP;
RETURN;
END;
$BODY$;

--------------------------------------------------------------------

CREATE OR REPLACE FUNCTION FX_SIS_ACT_CLAVE(p_ccorreo varchar,p_cclaveN varchar,p_codRecup varchar)
RETURNS TABLE(lresultado boolean, cmensaje varchar)
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	lncodigo INT;
BEGIN
	IF p_codRecup IS NOT NULL THEN
		SELECT A.ncodigo into lncodigo FROM sis_m_usuarios AS A WHERE UPPER(A.ccorreo) = UPPER(p_ccorreo) and A.ncodrecup = p_codRecup;
		IF lncodigo is null THEN
			lresultado = FALSE;
			cmensaje = 'CODIGO NO VALIDO PARA EL CORREO';
		ELSE
			UPDATE SIS_M_USUARIOS SET cclave = md5(p_cclaveN) , ncodrecup = NULL WHERE UPPER(ccorreo) = UPPER(p_ccorreo);
			lresultado = TRUE;
			cmensaje = 'SE ACTUALIZO SU CONTRASEÑA';
		END IF;
	ELSE
		lresultado = FALSE;
		cmensaje = 'CODIGO NO VALIDO PARA EL CORREO';
	END IF;
RETURN NEXT;
END;
$BODY$;
----------------------------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_sel_recuperacion(
	codrecup character varying)
    RETURNS TABLE(lresultado boolean, ccorreo character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
BEGIN
	IF codrecup IS NOT NULL THEN
		SELECT A.ccorreo INTO ccorreo FROM sis_m_usuarios AS A WHERE A.ncodrecup = codrecup;
		lresultado = TRUE;
	ELSE 
		lresultado = FALSE;
	END IF;
RETURN NEXT;
END;
$BODY$;

------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_ins_recuperacion(
	p_ccorreo character varying)
    RETURNS TABLE(lresultado boolean, codrecup character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	 codrecp varchar;
BEGIN
	codrecp = md5(random()::text);
	UPDATE SIS_M_USUARIOS SET ncodrecup = codrecp WHERE UPPER(ccorreo) = UPPER(p_ccorreo);
	codRecup = codrecp;
	lresultado = TRUE;
RETURN NEXT;
END;
$BODY$;

-----------------------------------------------------------------------------------------------------------------------------

--MANTENIMIENTO DE CATEGORIAS--
CREATE OR REPLACE FUNCTION public.fx_sis_ins_categoria(
	p_cnombre character varying)
    RETURNS TABLE(lresultado boolean, cmensaje character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
	 lncodigo int;
BEGIN
	Select ncodigo INTO lncodigo FROM sis_t_categoria WHERE cnombre =p_cnombre;
	IF lncodigo is null THEN
		INSERT INTO SIS_T_CATEGORIA(cnombre) VALUES (p_cnombre);
		lresultado = TRUE;
		cmensaje = 'CATEGORIA CREADA';
	ELSE 
		lresultado = FALSE;
		cmensaje = 'CATEGORIA YA EXISTENTE';
	END IF;

RETURN NEXT;
END;
$BODY$;
----------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_act_categoria(
	p_ncodcat int,p_cnombre character varying)
    RETURNS TABLE(lresultado boolean, cmensaje character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
BEGIN
	UPDATE SIS_T_CATEGORIA SET cnombre = p_cnombre WHERE ncodigo = p_ncodcat;
	lresultado= TRUE;
	cmensaje = 'CATEGORIA ACTUALIZADA';
RETURN NEXT;
END;
$BODY$;

------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_del_categoria(
	p_ncodcat int)
    RETURNS TABLE(lresultado boolean, cmensaje character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
BEGIN

	DELETE FROM SIS_T_CATEGORIA WHERE ncodigo = p_ncodcat;
	lresultado= TRUE;
	cmensaje = 'CATEGORIA ELIMINADA';
RETURN NEXT;
END;
$BODY$;
-------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_del_direccion(
	p_ncoddir int)
    RETURNS TABLE(lresultado boolean, cmensaje character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
    
AS $BODY$
DECLARE
BEGIN
	Delete From sis_p_direccion_cliente WHERE ncoddir = p_ncoddir;
	lresultado= TRUE;
	cmensaje = 'DIRECCION ELIMINADA';
	
RETURN NEXT;
END;
$BODY$;



