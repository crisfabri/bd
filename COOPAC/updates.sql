------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_updprovincia(
	integer,
	character,
	character varying,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_ncodigo_departamento ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario ALIAS FOR $6;
begin
	UPDATE	SIS_t_provincias
	SET	ccodigo = UPPER(p_ccodigo),
		cnombre = UPPER(p_cnombre),
		ncodigo_departamento = p_ncodigo_departamento,
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;


------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_sis_updpais(
	integer,
	character,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
	p_nusuario ALIAS FOR $5;
begin	
	UPDATE	SIS_t_paises
	SET	ccodigo = UPPER(p_ccodigo),
		cnombre = UPPER(p_cnombre),
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_upddepartamento(
	integer,
	character,
	character varying,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_ncodigo_pais ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario ALIAS FOR $6;
begin
	UPDATE	SIS_t_departamentos
	SET	ccodigo = UPPER(p_ccodigo),
		cnombre = UPPER(p_cnombre),
		ncodigo_pais = p_ncodigo_pais,
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_upddistrito(
	integer,
	character,
	character varying,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_ncodigo_provincia ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario ALIAS FOR $6;
begin
	UPDATE	sis_t_distritos
	SET	ccodigo = UPPER(p_ccodigo),
		cnombre = UPPER(p_cnombre),
		ncodigo_provincia = p_ncodigo_provincia,
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;




------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_updvariable(
	integer,
	character varying,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cvalor  ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario  ALIAS FOR $6;
	
begin
	UPDATE	sis_t_variables
	SET	ccodigo = UPPER(p_ccodigo),
		cnombre = UPPER(p_cnombre),
		cvalor = UPPER(p_cvalor),
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_tes_updconcepto(
	integer,
	character varying,
	character varying,
	boolean,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_cnombre ALIAS FOR $2;
	p_cctacnt ALIAS FOR $3;
	p_lafecaj ALIAS FOR $4;
	p_ncodofi ALIAS FOR $5;
	p_cestado  ALIAS FOR $6;
	p_nusuario  ALIAS FOR $7;
	
begin
	UPDATE	tes_t_conceptos
	SET	cnombre = UPPER(p_cnombre),
		cctacnt = p_cctacnt,
		lafecaj = p_lafecaj,
		ncodofi = p_ncodofi,
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_updterminal(
	integer,
	character varying,
	character,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_cip      ALIAS FOR $2;
	p_ccodigo  ALIAS FOR $3;
	p_ncodigo_oficina ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario ALIAS FOR $6;
begin	
	UPDATE	sis_t_terminales
	SET	cip = p_cip,
		ccodigo = UPPER(p_ccodigo),
		ncodigo_oficina = p_ncodigo_oficina,
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_sis_updprofesion(
	integer,
	character,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
	p_nusuario ALIAS FOR $5;
begin	
	UPDATE	SIS_t_profesiones
	SET	ccodigo = UPPER(p_ccodigo),
		cnombre = UPPER(p_cnombre),
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;
------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_updoficina(
	integer,
	character,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
	p_nusuario ALIAS FOR $5;
begin	
	UPDATE	SIS_t_oficinas
	SET	ccodigo = UPPER(p_ccodigo),
		cnombre = UPPER(p_cnombre),
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_updperito(
	integer,
	character varying,
	character,
	character,
	character,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_capellido_paterno ALIAS FOR $4;
	p_capellido_materno ALIAS FOR $5;
	p_cestado  ALIAS FOR $6;
	p_nusuario ALIAS FOR $7;
begin	
	UPDATE	sis_t_peritos
	SET	ccodigo = UPPER(p_ccodigo),
		cnombre = UPPER(p_cnombre),
		capellido_paterno = UPPER(p_capellido_paterno),
		capellido_materno = UPPER(p_capellido_materno),
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_updperfil(
	integer,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
	p_nusuario  ALIAS FOR $4;
begin	
	UPDATE	sis_t_perfiles
	SET	cnombre = UPPER(p_cnombre),
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_updopcion(
	integer,
	integer,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ncodigo_modulo ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_caccion  ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario  ALIAS FOR $6;
begin	
	UPDATE	sis_t_opciones
	SET	cnombre = UPPER(p_cnombre),
		caccion = p_caccion,
		ncodigo_modulo = p_ncodigo_modulo,
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_updmodulo(
	integer,
	character varying,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cicono  ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario  ALIAS FOR $6;
begin	
	UPDATE	sis_t_modulos
	SET	ccodigo = UPPER(p_ccodigo),
		cnombre = UPPER(p_cnombre),
		cicono = p_cicono,
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_updgarantia(
	integer,
	character,
	character varying,
	character,
	character,
	character,
	character,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cdescripcion  ALIAS FOR $3;	
	p_cpreferida  ALIAS FOR $4;	
	p_ccuenta_contable  ALIAS FOR $5;
	p_ccodigo_sbs  ALIAS FOR $6;	
	p_cdescripcion_corta  ALIAS FOR $7;	
	p_nperiodo_tasacion ALIAS FOR $8;	
	p_cestado  ALIAS FOR $9;
	p_nusuario ALIAS FOR $10;
	
begin	
	UPDATE	sis_t_garantias
	SET	ccodigo = UPPER(p_ccodigo),
		cdescripcion = UPPER(p_cdescripcion),
		cpreferida = UPPER(p_cpreferida),
		ccuenta_contable = UPPER(p_ccuenta_contable),
		ccodigo_sbs = UPPER(p_ccodigo_sbs),
		cdescripcion_corta = UPPER(p_cdescripcion_corta),
		nperiodo_tasacion = p_nperiodo_tasacion,
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;


------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_updactividad(
	integer,
	character,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
	p_nusuario ALIAS FOR $5;
begin	
	UPDATE	sis_t_actividades
	SET	ccodigo = UPPER(p_ccodigo),
		cnombre = UPPER(p_cnombre),
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_updferiado(
	integer,
	date,
	character,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_dfecha   ALIAS FOR $2;
	p_ctipo    ALIAS FOR $3;	
	p_cestado  ALIAS FOR $4;
	p_nusuario ALIAS FOR $5;
begin	
	UPDATE	sis_t_feriados
	SET	dfecha = p_dfecha,
		ctipo = p_ctipo,
		cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	where	ncodigo = p_ncodigo
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_updpperfil(
	integer,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo_usuario  ALIAS FOR $1;
	p_ncodigo_perfil  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
	p_nusuario  ALIAS FOR $4;
begin	
	UPDATE	sis_p_perfiles
	SET	cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	WHERE ncodigo_usuario = p_ncodigo_usuario AND ncodigo_perfil = p_ncodigo_perfil
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;


------------------------------------------------------------------------------------



CREATE OR REPLACE FUNCTION public.fx_sis_updppermiso(
	integer,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo_perfil  ALIAS FOR $1;
	p_ncodigo_opcion  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
	p_nusuario  ALIAS FOR $4;
begin	
	UPDATE	sis_p_permisos
	SET	cestado = p_cestado,
		nusuario_modifica = p_nusuario,
		tmodifica = now()
	WHERE ncodigo_perfil = p_ncodigo_perfil AND ncodigo_opcion = p_ncodigo_opcion
	;
	RETURN	query
	SELECT	'0'::CHARACTER as cCodTra,
		'Actualización Satisfactoria.'::CHARACTER VARYING AS cDesTra
	;
end
$BODY$;
