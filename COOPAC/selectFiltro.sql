
------------------------------- fx_sis_selpaises

CREATE OR REPLACE FUNCTION public.fx_sis_selpaises(
	character varying, character varying, character)
    RETURNS TABLE(ncodigo integer, ccodigo character varying, cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
begin
	RETURN	QUERY
	SELECT	pais.ncodigo,
			pais.ccodigo,
			pais.cnombre,
			pais.cestado
	from	sis_t_paises pais
	WHERE
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(pais.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(pais.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN pais.cestado = p_cestado
			ELSE TRUE
	END)
	order	by pais.ccodigo asc;
end
$BODY$;


--------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_seldistritos(
	character, character varying, integer, character)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character, ncodigo_provincia integer, cnomprovincia character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_ncodigo_provincia ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
begin	
	return	query
	select	dis.ncodigo,
		dis.ccodigo,
		dis.cnombre,
		dis.cestado,
		dis.ncodigo_provincia,
		pro.cnombre
	from	sis_t_distritos dis
		inner join sis_t_provincias pro on dis.ncodigo_provincia = pro.ncodigo
	WHERE		
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(dis.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(dis.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (p_ncodigo_provincia <> -1)
			THEN dis.ncodigo_provincia = p_ncodigo_provincia
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN dis.cestado = p_cestado
			ELSE TRUE
	END)
	;

end
$BODY$;



---------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_sis_selprovincias(
	character, character varying, integer, character)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character, ncodigo_departamento integer, cnomdep character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_ncodigo_departamento ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
	
begin	
	return	query
	select	pro.ncodigo,
		pro.ccodigo,
		pro.cnombre,
		pro.cestado,
		pro.ncodigo_departamento,
		dep.cnombre
	from	sis_t_provincias pro
		inner join sis_t_departamentos dep on pro.ncodigo_departamento = dep.ncodigo
	WHERE		
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(pro.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(pro.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (p_ncodigo_departamento <> -1)
			THEN pro.ncodigo_departamento = p_ncodigo_departamento
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN pro.cestado = p_cestado
			ELSE TRUE
	END)
	;

end
$BODY$;

-----------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_seldepartamentos(
	character, character varying, integer, character)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character, ncodigo_pais integer, cnompais character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_ncodigo_pais ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;	
begin	
	return	query
	select	dep.ncodigo,
		dep.ccodigo,
		dep.cnombre,
		dep.cestado,
		dep.ncodigo_pais,
		pais.cnombre
	from	sis_t_departamentos dep
		inner join sis_t_paises pais on dep.ncodigo_pais = pais.ncodigo
	WHERE		
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(dep.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(dep.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (p_ncodigo_pais <> -1)
			THEN dep.ncodigo_pais = p_ncodigo_pais
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN dep.cestado = p_cestado
			ELSE TRUE
	END)
	;

end
$BODY$;


---------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_tes_selconceptos(
	character varying, character varying, boolean, integer, character)
    RETURNS TABLE(ncodigo integer, cnombre character varying, cctacnt character varying, cestado character, lafecaj boolean, ncodofi integer, cnomofi character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_cnombre  ALIAS FOR $1;
	p_cctacnt  ALIAS FOR $2;
	p_lafecaj  ALIAS FOR $3;
	p_ncodofi  ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
begin
	RETURN	query
	select	con.ncodigo,
		con.cnombre,
		con.cctacnt,
		con.cestado,
		con.lafecaj,
		con.ncodofi,
		ofi.cnombre
	from	tes_t_conceptos con
		inner join sis_t_oficinas ofi on con.ncodofi = ofi.ncodigo
	WHERE		
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(con.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cctacnt))
			THEN TRIM(con.cctacnt) ilike '%'|| UPPER(TRIM(p_cctacnt)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN ( p_lafecaj IS NOT NULL)
			THEN con.lafecaj = p_lafecaj
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (p_ncodofi <> -1)
			THEN con.ncodofi = p_ncodofi
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN con.cestado = p_cestado
			ELSE TRUE
	END)
	;
	
end
$BODY$;

--------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selvariables(
	character varying, character varying, character)
    RETURNS TABLE(ncodigo integer, ccodigo character varying, cnombre character varying, cvalor character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
begin
	RETURN	QUERY
	select	var.ncodigo,
		var.ccodigo,
		var.cnombre,
		var.cvalor,
		var.cestado
	from	sis_t_variables var
	WHERE
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(var.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(var.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN var.cestado = p_cestado
			ELSE TRUE
	END)
	order	by var.ccodigo asc;
end
$BODY$;

----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_seloficinas(
	character varying,
	character varying,
	character)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
begin
	RETURN	QUERY
	SELECT	ofi.ncodigo,
			ofi.ccodigo,
			ofi.cnombre,
			ofi.cestado
	from	sis_t_oficinas ofi
	WHERE
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(ofi.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(ofi.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN ofi.cestado = p_cestado
			ELSE TRUE
	END)
	order	by ofi.ccodigo asc;
end
$BODY$;


----------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_selprofesiones(
	character varying, character varying, character)
    RETURNS TABLE(ncodigo integer, ccodigo character varying, cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
begin
	RETURN	QUERY
	SELECT	prof.ncodigo,
			prof.ccodigo,
			prof.cnombre,
			prof.cestado
	from	sis_t_profesiones prof
	WHERE
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(prof.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(prof.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN prof.cestado = p_cestado
			ELSE TRUE
	END)
	order	by prof.ccodigo asc;
end
$BODY$;

----------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_selterminales(
	character varying,
	character varying,
	integer,
	character)
    RETURNS TABLE(ncodigo integer, cip character varying, ccodigo character, cestado character, ncodigo_oficina integer, cnomofi character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cip  ALIAS FOR $2;
	p_ncodigo_oficina  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
begin
	RETURN	QUERY
	SELECT	ter.ncodigo,
		ter.cip,
		ter.ccodigo,
		ter.cestado,
		ter.ncodigo_oficina,
		ofi.cnombre 
	from	sis_t_terminales ter
		inner join sis_t_oficinas ofi on ter.ncodigo_oficina = ofi.ncodigo
	WHERE
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(ter.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cip))
			THEN TRIM(ter.cip) ilike '%'|| UPPER(TRIM(p_cip)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (p_ncodigo_oficina <> -1)
			THEN ter.ncodigo_oficina = p_ncodigo_oficina
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN TRIM(ter.cestado) = UPPER(TRIM(p_cestado))
			ELSE TRUE
	END)
	order	by ter.ccodigo asc;
end
$BODY$;

----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selperitos(
	character varying,
	character)
    RETURNS TABLE(ncodigo integer, ccodigo character varying, cnombre character, capellido_paterno character, capellido_materno character, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cestado  ALIAS FOR $2;
begin
	RETURN	QUERY
	SELECT	per.ncodigo,
			per.ccodigo,
			per.cnombre,
			per.capellido_paterno,
			per.capellido_materno,
			per.cestado
	from	sis_t_peritos per
	WHERE
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(per.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN TRIM(per.cestado) = UPPER(TRIM(p_cestado))
			ELSE TRUE
	END)
	order	by per.ccodigo asc;
end
$BODY$;


----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selperfiles(
	character varying,
	character)
    RETURNS TABLE(ncodigo integer, cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$DECLARE
	p_cnombre  ALIAS FOR $1;
	p_cestado  ALIAS FOR $2;
begin
	RETURN	QUERY
	SELECT	per.ncodigo,
			per.cnombre,
			per.cestado
	from	sis_t_perfiles per
	WHERE
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(per.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN TRIM(per.cestado) = UPPER(TRIM(p_cestado))
			ELSE TRUE
	END)
	;
end
$BODY$;



----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selopciones(
	integer, character varying, character)
    RETURNS TABLE(ncodigo integer, ncodigo_modulo integer, cnombre character varying, caccion character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo_modulo ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
begin
	RETURN	QUERY
	SELECT	opc.ncodigo,
			opc.ncodigo_modulo,
			opc.cnombre,
			opc.caccion,
			opc.cestado
	from	sis_t_opciones opc
	WHERE
	(CASE 
			WHEN (p_ncodigo_modulo <> -1)
			THEN opc.ncodigo_modulo = p_ncodigo_modulo
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(opc.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN TRIM(opc.cestado) = UPPER(TRIM(p_cestado))
			ELSE TRUE
	END)
	;
end
$BODY$;


----------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_sis_selmodulos(
	character varying,
	character varying,
	character)
    RETURNS TABLE(ncodigo integer, ccodigo character varying, cnombre character varying, cicono character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
begin
	RETURN	QUERY
	SELECT	modu.ncodigo,
			modu.ccodigo,
			modu.cnombre,
			modu.cicono,
			modu.cestado
	from	sis_t_modulos modu
	WHERE
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(modu.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(modu.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN TRIM(modu.cestado) = UPPER(TRIM(p_cestado))
			ELSE TRUE
	END)
	order	by modu.ccodigo asc;
end
$BODY$;




----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selactividades(
	character varying,
	character varying,
	character)
    RETURNS TABLE(ncodigo integer, ccodigo character varying, cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
begin
	RETURN	QUERY
	SELECT	act.ncodigo,
			act.ccodigo,
			act.cnombre,
			act.cestado
	from	sis_t_actividades act
		WHERE
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(act.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cnombre))
			THEN TRIM(act.cnombre) ilike '%'|| UPPER(TRIM(p_cnombre)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN TRIM(act.cestado) = UPPER(TRIM(p_cestado))
			ELSE TRUE
	END)
	order	by act.ccodigo asc;
end
$BODY$;



----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selferiados(
	character,
	date,
	date,
	character
	)
    RETURNS TABLE(ncodigo integer, dfecha date, ctipo character, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ctipo  ALIAS FOR $1;
	p_dfechaIni  ALIAS FOR $2;
	p_dfechaFin  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
begin
	RETURN	QUERY
	SELECT	fer.ncodigo,
			fer.dfecha,
			fer.ctipo,
			fer.cestado
	from	sis_t_feriados fer
	WHERE
	(CASE 
			WHEN (NOT fxempty(p_ctipo))
			THEN TRIM(fer.ctipo) ilike '%'|| UPPER(TRIM(p_ctipo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_dfechaIni))
			THEN fer.dfecha >= p_dfechaIni
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_dfechaFin))
			THEN fer.dfecha <= p_dfechaFin
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN fer.cestado = p_cestado
			ELSE TRUE
	END)
	order	by fer.ncodigo asc;
end
$BODY$;

----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selgarantias(
	character varying,
	character varying,
	character varying,
	character varying,
	character)
    RETURNS TABLE(ncodigo integer, ccodigo character, cdescripcion character varying, cpreferida character, ccuenta_contable character, ccodigo_sbs character, cdescripcion_corta character, nperiodo_tasacion integer, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cdescripcion  ALIAS FOR $2;
	p_cpreferida  ALIAS FOR $3;
	p_ccodigo_sbs  ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
begin
	RETURN	QUERY
	SELECT	gar.ncodigo,
			gar.ccodigo,
			gar.cdescripcion,
			gar.cpreferida,
			gar.ccuenta_contable,
			gar.ccodigo_sbs,
			gar.cdescripcion_corta,
			gar.nperiodo_tasacion,
			gar.cestado
	from	sis_t_garantias gar
	WHERE
	(CASE 
			WHEN (NOT fxempty(p_ccodigo))
			THEN TRIM(gar.ccodigo) ilike '%'|| UPPER(TRIM(p_ccodigo)) ||'%'
			ELSE TRUE
	END)
 	AND
	(CASE 
			WHEN (NOT fxempty(p_cdescripcion))
			THEN TRIM(gar.cdescripcion) ilike '%'|| UPPER(TRIM(p_cdescripcion)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cpreferida))
			THEN TRIM(gar.cpreferida) ilike '%'|| UPPER(TRIM(p_cpreferida)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_ccodigo_sbs))
			THEN TRIM(gar.ccodigo_sbs) ilike '%'|| UPPER(TRIM(p_ccodigo_sbs)) ||'%'
			ELSE TRUE
	END)
	AND
	(CASE 
			WHEN (NOT fxempty(p_cestado))
			THEN TRIM(gar.cestado) = UPPER(TRIM(p_cestado))
			ELSE TRUE
	END)
	order	by gar.ccodigo asc;
end
$BODY$;


----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------