CREATE OR REPLACE FUNCTION public.fx_sis_inspais(
	integer,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
	p_nusuario  ALIAS FOR $5;
	
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 4 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 4 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN
			IF EXISTS (SELECT ccodigo FROM sis_t_paises WHERE TRIM(ccodigo) = UPPER(TRIM(p_ccodigo))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			ELSIF EXISTS (SELECT cnombre FROM sis_t_paises WHERE TRIM(cnombre) = UPPER(TRIM(p_cnombre))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER varying AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_paises (
							ccodigo,
							cnombre,
							cestado,
							tcreacion,
							nusuario_crea)
				VALUES(		UPPER(p_ccodigo),
							UPPER(p_cnombre),
							p_cestado,
							now(),
							p_nusuario);

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;	
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updpais(
				p_ncodigo,
				p_ccodigo,
				p_cnombre,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;


------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_sis_instdepartamentos(
	integer,
	character,
	character varying,
	character,
	integer,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;	
	p_cestado  ALIAS FOR $4;
	p_ncodigo_pais ALIAS FOR $5;
	p_nusuario ALIAS FOR $6;
begin
    IF p_nCodigo = 0 THEN
		IF EXISTS (SELECT ccodigo FROM SIS_t_departamentos WHERE TRIM(ccodigo) = UPPER(TRIM(p_ccodigo))) THEN
			RETURN	query
			SELECT	'1'::CHARACTER as cCodTra,
				('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
			;
		ELSIF EXISTS (SELECT cnombre FROM SIS_t_departamentos WHERE TRIM(cnombre) = UPPER(TRIM(p_cnombre))) THEN
			RETURN	query
			SELECT	'1'::CHARACTER AS cCodTra,
				('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER VARYING AS cDesTra
			;
		ELSE
			INSERT INTO SIS_t_departamentos (ccodigo, cnombre, cestado, ncodigo_pais, tcreacion, nusuario_crea)
			VALUES(UPPER(p_ccodigo), UPPER(p_cnombre), p_cestado, p_ncodigo_pais, now(), p_nusuario  );

			RETURN	query
			SELECT	'0'::CHARACTER as cCodTra,
				'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
			;
		END IF;
	ELSE
		RETURN	query
		SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra FROM fx_sis_upddepartamento(
			p_ncodigo,
			p_ccodigo,
			p_cnombre,
			p_ncodigo_pais,
			p_cestado,
			p_nusuario) AS A
		;
	END IF;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_sis_insprovincia(
	integer,
	character varying,
	character varying,
	character,
	integer,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;	
	p_cestado  ALIAS FOR $4;
	p_ncodigo_departamento ALIAS FOR $5;
	p_nusuario ALIAS FOR $6;
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 4 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 4 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN
			IF EXISTS (SELECT ccodigo FROM sis_t_provincias WHERE TRIM(ccodigo) = UPPER(TRIM(p_ccodigo))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			ELSIF EXISTS (SELECT cnombre FROM sis_t_provincias WHERE TRIM(cnombre) = UPPER(TRIM(p_cnombre))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER AS cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER VARYING AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_provincias (ccodigo, cnombre, cestado, ncodigo_departamento, tcreacion, nusuario_crea)
				VALUES(UPPER(p_ccodigo), UPPER(p_cnombre), p_cestado, p_ncodigo_departamento, now(), p_nusuario  );

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra FROM fx_sis_updprovincia(
				p_ncodigo,
				p_ccodigo,
				p_cnombre,
				p_ncodigo_departamento,
				p_cestado,
				p_nusuario) AS A
			;
		END IF;
	END IF;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_sis_instdistrito(
	integer,
	character varying,
	character varying,
	character,
	integer,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;	
	p_cestado  ALIAS FOR $4;
	p_ncodigo_provincia ALIAS FOR $5;
	p_nusuario ALIAS FOR $6;
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 4 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 4 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN
			IF EXISTS (SELECT ccodigo FROM sis_t_distritos WHERE TRIM(ccodigo) = UPPER(TRIM(p_ccodigo))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			ELSIF EXISTS (SELECT cnombre FROM sis_t_distritos WHERE TRIM(cnombre) = UPPER(TRIM(p_cnombre))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER AS cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER VARYING AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_provincias (ccodigo, cnombre, cestado, ncodigo_provincia, tcreacion, nusuario_crea)
				VALUES(UPPER(p_ccodigo), UPPER(p_cnombre), p_cestado, p_ncodigo_provincia, now(), p_nusuario  );

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra FROM fx_sis_upddistrito(
				p_ncodigo,
				p_ccodigo,
				p_cnombre,
				p_ncodigo_provincia,
				p_cestado,
				p_nusuario) AS A
			;
		END IF;
	END IF;
end
$BODY$;
------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_insvariable(
	integer,
	character varying,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cvalor  ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario  ALIAS FOR $6;
	
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 20 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 20 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN
			IF EXISTS (SELECT ccodigo FROM sis_t_paises WHERE TRIM(ccodigo) = UPPER(TRIM(p_ccodigo))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			ELSIF EXISTS (SELECT cnombre FROM sis_t_paises WHERE TRIM(cnombre) = UPPER(TRIM(p_cnombre))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER varying AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_variables (ccodigo,
								cnombre,
								cvalor,								
								cestado,
								tcreacion,
								nusuario_crea)
				VALUES(			UPPER(p_ccodigo),
								UPPER(p_cnombre),
								UPPER(p_cvalor),								
								p_cestado,
								now(),
								p_nusuario);


				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;	
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updvariable(
				p_ncodigo,
				p_ccodigo,
				p_cnombre,
				p_cvalor,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_tes_insconcepto(
	integer,
	character varying,
	character varying,
	boolean,
	integer,
	character,
	integer)
	
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_cnombre ALIAS FOR $2;
	p_cctacnt ALIAS FOR $3;
	p_lafecaj ALIAS FOR $4;
	p_ncodofi ALIAS FOR $5;
	p_cestado  ALIAS FOR $6;
	p_nusuario  ALIAS FOR $7;
	
begin
	IF fxempty(p_cctacnt) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_cctacnt].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN
			IF EXISTS (SELECT cnombre FROM tes_t_conceptos WHERE TRIM(UPPER(cnombre)) = TRIM(p_cnombre)) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER varying AS cDesTra
				;
			ELSE
				INSERT INTO tes_t_conceptos (	cnombre,
												cctacnt,
												cestado,
												tcreacion,
												nusuario_crea,
												lafecaj,
												ncodofi)
								VALUES(			UPPER(p_cnombre),
												UPPER(p_cctacnt),
												p_cestado,
												now(),
												p_nusuario,
												p_lafecaj,
												p_ncodofi);

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;	
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_tes_updconcepto(
				p_ncodigo,
				p_cnombre,
				p_cctacnt,
				p_lafecaj,
				p_ncodofi,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;
------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_sis_insterminal(
	integer,
	character varying,
	character,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_cip      ALIAS FOR $2;
	p_ccodigo  ALIAS FOR $3;
	p_ncodigo_oficina ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario ALIAS FOR $6;
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cip) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cip].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 4 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 4 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN
			IF EXISTS (SELECT ccodigo FROM sis_t_terminales WHERE TRIM(ccodigo) = UPPER(TRIM(p_ccodigo))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_terminales (	
								cip,
								ccodigo,
								ncodigo_oficina,								
								cestado,
								tcreacion,
								nusuario_crea)
				VALUES(			UPPER(p_cip),
								UPPER(p_ccodigo),
								p_ncodigo_oficina,								
								p_cestado,
								now(),
								p_nusuario);

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;	
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updterminal(
				p_ncodigo,
				p_cip,
				p_ccodigo,
				p_ncodigo_oficina,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;


------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION public.fx_sis_insprofesion(
	integer,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
	p_nusuario  ALIAS FOR $5;
	
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 5 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 5 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN
			IF EXISTS (SELECT ccodigo FROM sis_t_profesiones WHERE TRIM(ccodigo) = UPPER(TRIM(p_ccodigo))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			ELSIF EXISTS (SELECT cnombre FROM sis_t_profesiones WHERE TRIM(cnombre) = UPPER(TRIM(p_cnombre))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER varying AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_profesiones (
							ccodigo,
							cnombre,
							cestado,
							tcreacion,
							nusuario_crea)
				VALUES(		UPPER(p_ccodigo),
							UPPER(p_cnombre),
							p_cestado,
							now(),
							p_nusuario);

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;	
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updprofesion(
				p_ncodigo,
				p_ccodigo,
				p_cnombre,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;
------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_insoficina(
	integer,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
	p_nusuario  ALIAS FOR $5;
	
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 4 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 4 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN
			IF EXISTS (SELECT ccodigo FROM sis_t_oficinas WHERE TRIM(ccodigo) = UPPER(TRIM(p_ccodigo))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			ELSIF EXISTS (SELECT cnombre FROM sis_t_oficinas WHERE TRIM(cnombre) = UPPER(TRIM(p_cnombre))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER varying AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_oficinas (
							ccodigo,
							cnombre,
							cestado,
							tcreacion,
							nusuario_crea)
				VALUES(		UPPER(p_ccodigo),
							UPPER(p_cnombre),
							p_cestado,
							now(),
							p_nusuario);

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;	
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updoficina(
				p_ncodigo,
				p_ccodigo,
				p_cnombre,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_insperito(
	integer,
	character varying,
	character varying,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_capellido_paterno ALIAS FOR $4;
	p_capellido_materno ALIAS FOR $5;
	p_cestado  ALIAS FOR $6;
	p_nusuario  ALIAS FOR $7;
	
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_capellido_paterno) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_capellido_paterno].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 20 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 20 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN
			IF EXISTS (SELECT ccodigo FROM sis_t_peritos WHERE TRIM(ccodigo) = UPPER(TRIM(p_ccodigo))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			ELSIF EXISTS (SELECT cnombre FROM sis_t_peritos WHERE TRIM(cnombre) = UPPER(TRIM(p_cnombre)) AND TRIM(capellido_paterno) = UPPER(TRIM(p_capellido_paterno)) AND TRIM(capellido_materno) = UPPER(TRIM(p_capellido_materno))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER AS cCodTra,
					('Registro Duplicado ['|| p_cnombre || ' ' || p_capellido_paterno || ' ' || p_capellido_materno||'].')::CHARACTER VARYING AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_peritos (
					ccodigo, 
					cnombre, 
					cestado, 
					capellido_paterno, 
					capellido_materno, 
					tcreacion, 
					nusuario_crea)
				VALUES( 
					UPPER(p_ccodigo), 
					UPPER(p_cnombre), 
					p_cestado, 
					UPPER(p_capellido_paterno),
					UPPER(p_capellido_materno),
					now(), 
					p_nusuario  );
				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;	
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updperito(
				p_ncodigo,
				p_ccodigo,
				p_cnombre,
				p_capellido_paterno,
				p_capellido_materno,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;


------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_insperfil(
	integer,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
	p_nusuario  ALIAS FOR $4;
	
begin
	IF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN	
			IF EXISTS (SELECT cnombre FROM sis_t_perfiles WHERE TRIM(cnombre) = UPPER(TRIM(p_cnombre))) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER varying AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_perfiles (
							cnombre,
							cestado,
							tcreacion,
							nusuario_crea)
				VALUES(		UPPER(p_cnombre),
							p_cestado,
							now(),
							p_nusuario);

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;	
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updperfil(
				p_ncodigo,
				p_cnombre,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_insopcion(
	integer,
	integer,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ncodigo_modulo ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_caccion  ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario  ALIAS FOR $6;
	
begin
	IF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN	
			IF EXISTS (SELECT cnombre FROM sis_t_opciones WHERE cnombre = p_cnombre) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER varying AS cDesTra
				;
			ELSIF EXISTS (SELECT caccion FROM sis_t_opciones WHERE caccion = p_caccion) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_caccion ||'].')::CHARACTER varying AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_opciones (
							ncodigo_modulo,
							cnombre,
							caccion,
							cestado,
							tcreacion,
							nusuario_crea)
				VALUES(		p_ncodigo_modulo,
							UPPER(p_cnombre),
							p_caccion,
							p_cestado,
							now(),
							p_nusuario);

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updopcion(
				p_ncodigo,
				p_ncodigo_modulo,
				p_cnombre,
				p_caccion,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;
------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_insmodulo(
	integer,
	character varying,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;
	p_cicono  ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario  ALIAS FOR $6;
	
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 10 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 10 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN	
			IF EXISTS (SELECT ccodigo FROM sis_t_modulos WHERE ccodigo = p_ccodigo) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			elsIF EXISTS (SELECT cnombre FROM sis_t_modulos WHERE cnombre = p_cnombre) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER varying AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_modulos (
							ccodigo,
							cnombre,
							cicono,
							cestado,
							tcreacion,
							nusuario_crea)
				VALUES(		p_ccodigo,
							p_cnombre,
							p_cicono,
							p_cestado,
							now(),
							p_nusuario);

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updmodulo(
				p_ncodigo,
				p_ccodigo,
				p_cnombre,
				p_cicono,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_insgarantia(
	integer,
	character,
	character varying,
	character,
	character,
	character,
	character,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cdescripcion  ALIAS FOR $3;	
	p_cpreferida  ALIAS FOR $4;	
	p_ccuenta_contable  ALIAS FOR $5;
	p_ccodigo_sbs  ALIAS FOR $6;	
	p_cdescripcion_corta  ALIAS FOR $7;	
	p_nperiodo_tasacion ALIAS FOR $8;	
	p_cestado  ALIAS FOR $9;
	p_nusuario ALIAS FOR $10;
	
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 3 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 3 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cpreferida) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_cpreferida].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_ccuenta_contable) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_ccuenta_contable].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_ccodigo_sbs) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_ccodigo_sbs].')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cdescripcion_corta) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_cdescripcion_corta].')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN	
			IF EXISTS (SELECT ccodigo FROM sis_t_garantias WHERE ccodigo = p_ccodigo) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			ELSIF EXISTS (SELECT cdescripcion_corta FROM sis_t_garantias WHERE cdescripcion_corta = p_cdescripcion_corta) THEN
				RETURN	query
				SELECT	'1'::CHARACTER AS cCodTra,
					('Registro Duplicado ['|| p_cdescripcion_corta ||'].')::CHARACTER VARYING AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_garantias (
					ccodigo, 
					cdescripcion, 
					cpreferida, 
					ccuenta_contable, 
					ccodigo_sbs, 
					cdescripcion_corta, 
					nperiodo_tasacion, 
					cestado, 
					tcreacion, 
					nusuario_crea)
				VALUES(p_ccodigo, 
					   p_cdescripcion, 
					   p_cpreferida, 
					   p_ccuenta_contable, 
					   p_ccodigo_sbs, 
					   p_cdescripcion_corta, 
					   p_nperiodo_tasacion, 
					   p_cestado, now(), 
					   p_nusuario  );

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updgarantia(
				p_ncodigo,
				p_ccodigo, 
				p_cdescripcion, 
				p_cpreferida, 
				p_ccuenta_contable, 
				p_ccodigo_sbs, 
				p_cdescripcion_corta, 
				p_nperiodo_tasacion, 
				p_cestado, 
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;


------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_insferiado(
	integer,
	date,
	character,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_dfecha   ALIAS FOR $2;
	p_ctipo    ALIAS FOR $3;	
	p_cestado  ALIAS FOR $4;
	p_nusuario ALIAS FOR $5;
begin
	IF fxempty(p_dfecha) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [fecha].')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN	
			IF EXISTS (SELECT dfecha FROM sis_t_feriados WHERE dfecha = p_dfecha) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_dfecha ||'].')::CHARACTER varying AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_feriados (
							dfecha,
							ctipo,
							cestado,
							tcreacion,
							nusuario_crea)
				VALUES(			p_dfecha,					
							p_ctipo,
							p_cestado,
							now(),
							p_nusuario);

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updferiado(
				p_ncodigo,
				p_dfecha, 
				p_ctipo, 
				p_cestado, 
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;

------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_insactividad(
	integer,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
	p_ccodigo  ALIAS FOR $2;
	p_cnombre  ALIAS FOR $3;	
	p_cestado  ALIAS FOR $4;
	p_nusuario ALIAS FOR $5;
begin
	IF fxempty(p_ccodigo) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [ccodigo].')::CHARACTER varying AS cDesTra
		;
	ELSIF LENGTH (p_ccodigo) > 6 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Codigo ['|| p_ccodigo ||'] no puede exceder 6 caracteres.')::CHARACTER varying AS cDesTra
		;
	ELSIF fxempty(p_cnombre) then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_cnombre].')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF p_nCodigo = 0 THEN	
			IF EXISTS (SELECT ccodigo FROM sis_t_actividades WHERE ccodigo = p_ccodigo) THEN
				RETURN	query
				SELECT	'1'::CHARACTER as cCodTra,
					('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
				;
			ELSIF EXISTS (SELECT cnombre FROM sis_t_actividades WHERE cnombre = p_cnombre) THEN
				RETURN	query
				SELECT	'1'::CHARACTER AS cCodTra,
					('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER VARYING AS cDesTra
				;
			ELSE
				INSERT INTO sis_t_actividades (
						ccodigo, 
						cnombre, 
						cestado, 
						tcreacion, 
						nusuario_crea)
				VALUES(		p_ccodigo,
						   p_cnombre, 
						   p_cestado, 
						   now(), 
						   p_nusuario  );

				RETURN	query
				SELECT	'0'::CHARACTER as cCodTra,
					'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
				;
			END IF;
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updactividad(
				p_ncodigo,
				p_ccodigo, 
				p_cnombre, 
				p_cestado, 
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;




--------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_inspperfil(
	integer,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo_usuario  ALIAS FOR $1;
	p_ncodigo_perfil  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
	p_nusuario  ALIAS FOR $4;
	
begin
	IF p_ncodigo_usuario = 0 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_ncodigo_usuario].')::CHARACTER varying AS cDesTra
		;
	ELSIF p_ncodigo_perfil = 0 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_ncodigo_perfil].')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF NOT EXISTS (SELECT ncodigo FROM sis_p_perfiles WHERE ncodigo_usuario = p_ncodigo_usuario AND ncodigo_perfil = p_ncodigo_perfil) THEN
			INSERT INTO sis_p_perfiles (
						ncodigo_usuario,
						ncodigo_perfil,
						cestado,
						tcreacion,
						nusuario_crea)
			VALUES(		p_ncodigo_usuario,
						p_ncodigo_perfil,
						p_cestado,
						now(),
						p_nusuario);

			RETURN	query
			SELECT	'0'::CHARACTER as cCodTra,
				'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
			;
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updpperfil(
				p_ncodigo_usuario,
				p_ncodigo_perfil,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;

--------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_insppermiso(
	integer,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo_opcion  ALIAS FOR $1;
	p_ncodigo_perfil  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
	p_nusuario  ALIAS FOR $4;
	
begin
	IF p_ncodigo_perfil = 0 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_ncodigo_perfil].')::CHARACTER varying AS cDesTra
		;
	ELSIF p_ncodigo_opcion = 0 then 
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Atributo vacio [p_ncodigo_opcion].')::CHARACTER varying AS cDesTra
		;
	ELSE
		IF NOT EXISTS (SELECT ncodigo FROM sis_p_permisos WHERE ncodigo_perfil = p_ncodigo_perfil AND ncodigo_opcion = p_ncodigo_opcion) THEN
			INSERT INTO sis_p_permisos (
						ncodigo_perfil,
						ncodigo_opcion,
						cestado,
						tcreacion,
						nusuario_crea)
			VALUES(		p_ncodigo_perfil,
						p_ncodigo_opcion,
						p_cestado,
						now(),
						p_nusuario);

			RETURN	query
			SELECT	'0'::CHARACTER as cCodTra,
				'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
			;
		ELSE
			RETURN	query
			SELECT A.cCodTra as cCodTra, A.cDesTra as cDesTra 
			FROM fx_sis_updppermiso(
				p_ncodigo_perfil,
				p_ncodigo_opcion,
				p_cestado,
				p_nusuario) as A
			;
		END IF;
	END IF;
end
$BODY$;