			 

CREATE OR REPLACE FUNCTION public.fx_aho_inactproducto(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM aho_m_productos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE aho_m_productos 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_aho_actproducto(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM aho_m_productos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE aho_m_productos 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;
			
--------------------------------------------------------------------------------------------------------------------
			 
			 

CREATE OR REPLACE FUNCTION public.fx_aho_inactproductosbs(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM aho_m_productossbs WHERE ncodigo = p_ncodigo ) THEN
		UPDATE aho_m_productossbs 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_aho_actproductosbs(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM aho_m_productossbs WHERE ncodigo = p_ncodigo ) THEN
		UPDATE aho_m_productossbs 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;
			 
-----------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_aho_inactcontabilidad(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM aho_t_contabilidad WHERE ncodigo = p_ncodigo ) THEN
		UPDATE aho_t_contabilidad 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_aho_actcontabilidad(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM aho_t_contabilidad WHERE ncodigo = p_ncodigo ) THEN
		UPDATE aho_t_contabilidad 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;
			 
------------------------------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_aho_inacttarifario(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM aho_t_tarifario WHERE ncodigo = p_ncodigo ) THEN
		UPDATE aho_t_tarifario 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_aho_acttarifario(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM aho_t_tarifario WHERE ncodigo = p_ncodigo ) THEN
		UPDATE aho_t_tarifario 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;
			 
------------------------------------------------------------------------------------------------------------------------



CREATE OR REPLACE FUNCTION public.fx_caj_inactconcepto(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM caj_t_conceptos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE caj_t_conceptos 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_caj_actconcepto(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM caj_t_conceptos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE caj_t_conceptos 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;
			 
			 

----------------------------------------------------------------------------------------------------------------------------





CREATE OR REPLACE FUNCTION public.fx_caj_inactcontabilidad(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM caj_t_contabilidad WHERE ncodigo = p_ncodigo ) THEN
		UPDATE caj_t_contabilidad 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_caj_actcontabilidad(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM caj_t_contabilidad WHERE ncodigo = p_ncodigo ) THEN
		UPDATE caj_t_contabilidad 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;



------------------------------------------------------------------------------------------------------------------





CREATE OR REPLACE FUNCTION public.fx_cli_inactconvenios(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cli_t_convenios WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cli_t_convenios 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cli_actconvenios(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cli_t_convenios WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cli_t_convenios 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;


-------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cnt_inactbalancesbs(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cnt_t_balancesbs WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cnt_t_balancesbs 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cnt_actbalancesbs(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cnt_t_balancesbs WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cnt_t_balancesbs 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

-----------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cre_inactproducto(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_m_productos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_m_productos 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cre_actproducto(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_m_productos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_m_productos 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;



---------------------------------------------------------------------------------------------------



CREATE OR REPLACE FUNCTION public.fx_cre_inactproductosbs(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_m_productossbs WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_m_productossbs 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cre_actproductosbs(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_m_productossbs WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_m_productossbs 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

-------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cre_inactautonomias(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_t_autonomias WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_t_autonomias 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cre_actautonomias(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_t_autonomias WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_t_autonomias 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_cre_inactcalificacion(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_t_calificacion WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_t_calificacion 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cre_actcalificacion(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_t_calificacion WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_t_calificacion 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;


---------------------------------------------------------------------------------------------------------------------------------



CREATE OR REPLACE FUNCTION public.fx_cre_inactcontabilidad(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_t_contabilidad WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_t_contabilidad 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cre_actcontabilidad(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_t_contabilidad WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_t_contabilidad 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

----------------------------------------------------------------------------------------------



CREATE OR REPLACE FUNCTION public.fx_cre_inactprovision(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_t_provision WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_t_provision 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cre_actprovision(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_t_provision WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_t_provision 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cre_inacttarifario(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_t_tarifario WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_t_tarifario 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_cre_acttarifario(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM cre_t_tarifario WHERE ncodigo = p_ncodigo ) THEN
		UPDATE cre_t_tarifario 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;
----------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_inactusuario(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_m_usuarios WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_m_usuarios 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actusuario(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_m_usuarios WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_m_usuarios 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;
--------------------------------------------------------------------------------------------------------------





CREATE OR REPLACE FUNCTION public.fx_sis_inactactividades(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_actividades WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_actividades 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actactividades(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_actividades WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_actividades 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

---------------------------------------------------------------------------------------------------------







CREATE OR REPLACE FUNCTION public.fx_sis_inactdeparatamento(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_departamentos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_departamentos 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actdeparatamento(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_departamentos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_departamentos 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

---------------------------------------------------------------









CREATE OR REPLACE FUNCTION public.fx_sis_inactdistritos(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_distritos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_distritos 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actdistritos(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_distritos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_distritos 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;
----------------------------------------------------------------------------------------










CREATE OR REPLACE FUNCTION public.fx_sis_inactferiados(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_feriados WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_feriados 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actferiados(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_feriados WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_feriados 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;


--------------------------------------------------------------------------------------------------------








CREATE OR REPLACE FUNCTION public.fx_sis_inactgarantia(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_garantias WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_garantias 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actgarantia(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_garantias WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_garantias 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;


---------------------------------------------------------------------






CREATE OR REPLACE FUNCTION public.fx_sis_inactmodulo(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_modulos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_modulos 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actmodulo(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_modulos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_modulos 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------------------------------------




CREATE OR REPLACE FUNCTION public.fx_sis_inactoficina(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_oficinas WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_oficinas 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actoficina(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_oficinas WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_oficinas 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;



------------------------------------------------------------------------------------------------------------------









CREATE OR REPLACE FUNCTION public.fx_sis_inactopcion(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_opciones WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_opciones 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actopcion(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_opciones WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_opciones 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;


------------------------------------------------------------------------------------------------






CREATE OR REPLACE FUNCTION public.fx_sis_inactpais(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_paises WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_paises 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actpais(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_paises WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_paises 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;


-------------------------------------------------------------------------------------------------------------------------------







CREATE OR REPLACE FUNCTION public.fx_sis_inactperfil(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_perfiles WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_perfiles 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actperfil(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_perfiles WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_perfiles 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

----------------------------------------------------------------------------------------------------------------------------------------











CREATE OR REPLACE FUNCTION public.fx_sis_inactperito(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_peritos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_peritos 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actperito(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_peritos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_peritos 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;
------------------------------------------------------------------------------------------------------------------------------------------








CREATE OR REPLACE FUNCTION public.fx_sis_inactprofesion(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_profesiones WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_profesiones 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actprofesion(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_profesiones WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_profesiones 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

-------------------------------------------------------------------------------------------------------------------------------------------------







CREATE OR REPLACE FUNCTION public.fx_sis_inactprovincias(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_provincias WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_provincias 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actprovincias(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_provincias WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_provincias 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------











CREATE OR REPLACE FUNCTION public.fx_sis_inactterminal(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_terminales WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_terminales 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actterminal(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_terminales WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_terminales 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;


------------------------------------------------------------------------------------------------------------------------------------------










CREATE OR REPLACE FUNCTION public.fx_sis_inactvariable(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_variables WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_variables 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_actvariable(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM sis_t_variables WHERE ncodigo = p_ncodigo ) THEN
		UPDATE sis_t_variables 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;





------------------------------------------------------------------------------------------------------------------------------------------------------------
















CREATE OR REPLACE FUNCTION public.fx_tes_inactconcepto(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM tes_t_conceptos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE tes_t_conceptos 
		set cestado = 'I' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Inactivado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_tes_actconcepto(
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	IF EXISTS(Select cestado FROM tes_t_conceptos WHERE ncodigo = p_ncodigo ) THEN
		UPDATE tes_t_conceptos 
		set cestado = 'A' 
		WHERE ncodigo = p_ncodigo;

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Activado.'::CHARACTER VARYING AS cDesTra
		;
	ELSE
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Inexistente '::CHARACTER VARYING) AS cDesTra
		;
	END IF;
end
$BODY$;

-----------------------------------------------------------------------------------------------------------------------------------









----------------------------------------------------------------------------INSERT--------------------------------------------------------------





----------------------------------------------------------------------------------------------------------------------------------





CREATE OR REPLACE FUNCTION public.fx_aho_insproductosbs(
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;	
	p_cestado  ALIAS FOR $3;
	p_nusuario ALIAS FOR $4;
begin
	IF EXISTS (SELECT ccodigo FROM cle_m_productosbs WHERE ccodigo = p_ccodigo) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSIF EXISTS (SELECT cnombre FROM cle_m_productosbs WHERE cnombre = p_cnombre) THEN
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER VARYING AS cDesTra
		;
	ELSE
		INSERT INTO cle_m_productosbs (ccodigo, cnombre, cestado, tcreacion, nusuario_crea)
		VALUES(p_ccodigo, p_cnombre, p_cestado, now(), p_nusuario  );

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;

---------------------------------------------------------------------------------------------------------




CREATE OR REPLACE FUNCTION public.fx_caj_inscontabilidad(
	character varying,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_cllave  ALIAS FOR $1;
	p_cdescripcion  ALIAS FOR $2;	
	p_ccuenta_contable  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
	p_nusuario ALIAS FOR $5;
begin
	IF EXISTS (SELECT cllave FROM caj_t_contabilidad WHERE cllave = p_cllave) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSE
		INSERT INTO caj_t_contabilidad (cllave, cdescripcion, ccuenta_contable,cestado,tcreacion, nusuario_crea)
		VALUES(p_cllave, p_cdescripcion, p_ccuenta_contable,p_cestado, now(), p_nusuario  );

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;


------------------------------------------------------------------------------------------------------------------



CREATE OR REPLACE FUNCTION public.fx_aho_inscontabilidad(
	character varying,
	integer,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_cllave  ALIAS FOR $1;
	p_ncodigo_producto  ALIAS FOR $2;	
	p_cdescripcion  ALIAS FOR $3;
	p_ccuenta_contable  ALIAS FOR $4;
	p_cestado  ALIAS FOR $5;
	p_nusuario ALIAS FOR $6;
begin
	IF EXISTS (SELECT cllave FROM aho_t_contabilidad WHERE cllave = p_cllave) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSE
		INSERT INTO aho_t_contabilidad (cllave, ncodigo_producto, cdescripcion,ccuenta_contable,cestado,tcreacion, nusuario_crea)
		VALUES(p_cllave, p_ncodigo_producto, p_cdescripcion,p_ccuenta_contable,p_cestado, now(), p_nusuario  );

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;


-------------------------------------------------------------------------------------------------------------------





CREATE OR REPLACE FUNCTION public.fx_aho_instarifario(
	integer,
	character,
	integer,
	integer,
	numeric,
	numeric,
	numeric,
	numeric,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo_producto  ALIAS FOR $1;
	p_cmoneda  ALIAS FOR $2;
	p_nplazo_minimo  ALIAS FOR $3;
	p_nplazo_maximo  ALIAS FOR $4;
	p_nmonto_minimo  ALIAS FOR $5;
	p_nmonto_maximo  ALIAS FOR $6;
	p_ntea_minima  ALIAS FOR $7;
	p_ntea_maxima  ALIAS FOR $8;
	p_cestado  ALIAS FOR $9;
	p_nusuario ALIAS FOR $10;
begin
		INSERT INTO aho_t_tarifario (ncodigo_producto, cmoneda, nplazo_minimo,nplazo_maximo,nmonto_minimo,nmonto_maximo,ntea_minima,ntea_maxima,cestado,tcreacion, nusuario_crea)
		VALUES(p_ncodigo_producto, p_cmoneda, p_nplazo_minimo,p_nplazo_maximo,p_nmonto_minimo,p_nmonto_maximo,p_ntea_minima,p_ntea_maxima,p_cestado, now(), p_nusuario  );

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
end
$BODY$;

-------------------------------------------------------------------------------------------------------------------------------





CREATE OR REPLACE FUNCTION public.fx_caj_insconceptos(
	character varying,
	character,
	character,
	character,
	numeric,
	character varying,
	character,
	character,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_ctipo_operacion  ALIAS FOR $2;
	p_ctipo_pago  ALIAS FOR $3;
	p_cmoneda  ALIAS FOR $4;
	p_nmonto  ALIAS FOR $5;
	p_cdescripcion  ALIAS FOR $6;
	p_ccuenta_debe  ALIAS FOR $7;
	p_ccuenta_haber  ALIAS FOR $8;
	p_cestado  ALIAS FOR $9;
	p_nusuario ALIAS FOR $10;
begin
	IF EXISTS (SELECT ccodigo FROM caj_t_conceptos WHERE ccodigo = p_ccodigo) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSE
		INSERT INTO caj_t_conceptos (ccodigo, ctipo_operacion, ctipo_pago,cmoneda,nmonto,cdescripcion,ccuenta_debe,ccuenta_haber,cestado,tcreacion, nusuario_crea)
		VALUES(p_ccodigo, p_ctipo_operacion, p_ctipo_pago,p_cmoneda,p_nmonto,p_cdescripcion,p_ccuenta_debe,p_ccuenta_haber,p_cestado, now(), p_nusuario  );

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;

-----------------------------------------------------------------------------------------------------------------------------------





CREATE OR REPLACE FUNCTION public.fx_caj_inscontabilidad(
	character varying,
	character varying,
	character,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_cllave ALIAS FOR $1;
	p_cdescripcion  ALIAS FOR $2;
	p_ccuenta_contable  ALIAS FOR $3;
	p_cestado  ALIAS FOR $4;
	p_nusuario ALIAS FOR $5;
begin
	IF EXISTS (SELECT cllave FROM caj_t_contabilidad WHERE cllave = p_cllave) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSE
		INSERT INTO caj_t_contabilidad (cllave, cdescripcion, ccuenta_contable,cestado,tcreacion, nusuario_crea)
		VALUES(p_cllave, p_cdescripcion, p_ccuenta_contable,p_cestado, now(), p_nusuario  );

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;
--------------------------------------------------------------------------------------------------------------------------------------------------






CREATE OR REPLACE FUNCTION public.fx_cli_insconvenios(
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;
	p_cestado  ALIAS FOR $3;
	p_nusuario ALIAS FOR $4;
begin
	IF EXISTS (SELECT cllave FROM cli_t_convenios WHERE cllave = p_cllave) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSE
		INSERT INTO cli_t_convenios (ccodigo, cnombre,cestado,tcreacion, nusuario_crea)
		VALUES(p_ccodigo, p_cnombre, p_cestado, now(), p_nusuario  );

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;

-------------------------------------------------------------------------------------------------------------------------------------------------------









CREATE OR REPLACE FUNCTION public.fx_cnt_insbalancesbs(
	character varying)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccuenta_contable ALIAS FOR $1;
begin
	IF EXISTS (SELECT ccuenta_contable FROM cnt_t_balancesbs WHERE ccuenta_contable = p_ccuenta_contable) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSE
		INSERT INTO cnt_t_balancesbs (ccuenta_contable)
		VALUES(p_ccuenta_contable);

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;
--------------------------------------------------------------------------------------------------------------------------------



CREATE OR REPLACE FUNCTION public.fx_cre_insproducto(
	character varying,
	character varying,
	character varying,
	numeric,
	numeric,
	integer,
	integer,
	date,
	date,
	character,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo ALIAS FOR $1;
	p_cservicio ALIAS FOR $2;
	p_cnombre ALIAS FOR $3;
	p_nmonto_minimo ALIAS FOR $4;
	p_nmonto_maximo ALIAS FOR $5;
	p_nplazo_minimo ALIAS FOR $6;
	p_nplazo_maximo ALIAS FOR $7;
	p_dinicio ALIAS FOR $8;
	p_dfin ALIAS FOR $12;
	p_cdestino ALIAS FOR $9;
	p_ncodigo_convenio ALIAS FOR $10;
	p_cestado ALIAS FOR $11;
	p_nusuario_crea ALIAS FOR $13;
begin
	IF EXISTS (SELECT ccodigo FROM cre_m_productos WHERE ccodigo = p_ccodigo) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSE IF EXISTS (SELECT cnombre FROM cre_m_productos WHERE cnombre = p_cnombre) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSE
		INSERT INTO cre_m_productos (ccodigo,cservicio,cnombre,nmonto_minimo,nplazo_minimo,nplazo_minimo,nplazo_maximo,dinicio,cdestino,ncodigo_convenio,cestado,nusuario_crea)
		VALUES(p_ccodigo,p_cservicio,p_cnombre,p_nmonto_minimo,p_nmonto_maximo,p_nplazo_minimo,p_nplazo_maximo,p_dinicio,p_dfin,p_cdestino,p_ncodigo_convenio,p_cestado,p_nusuario_crea);

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;









---------------------------- fx_sis_seldepartamentos

CREATE OR REPLACE FUNCTION public.fx_sis_seldepartamentos(
	character varying)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character, ncodigo_pais integer, cnompais character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
		
	p_cvalbus     ALIAS FOR $1;
	
begin	
	return	query
	select	dep.ncodigo,
		dep.ccodigo,
		dep.cnombre,
		dep.cestado,
		dep.ncodigo_pais,
		pais.cnombre
	from	sis_t_departamentos dep
		inner join sis_t_paises pais on dep.ncodigo_pais = pais.ncodigo
	where	(dep.ccodigo ilike '%'|| p_cvalbus ||'%' or dep.cnombre ilike '%'|| p_cvalbus ||'%')

	;

end
$BODY$;



---------------------------------------------fx_sis_selprovincias

CREATE OR REPLACE FUNCTION public.fx_sis_selprovincias(
	character varying)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character, ncodigo_departamento integer, cnomdep character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
		
	p_cvalbus     ALIAS FOR $1;
	
begin	
	return	query
	select	pro.ncodigo,
		pro.ccodigo,
		pro.cnombre,
		pro.cestado,
		pro.ncodigo_departamento,
		dep.cnombre
	from	sis_t_provincias pro
		inner join sis_t_departamentos dep on pro.ncodigo_departamento = dep.ncodigo
	where	(pro.ccodigo ilike '%'|| p_cvalbus ||'%' or pro.cnombre ilike '%'|| p_cvalbus ||'%')

	;

end
$BODY$;




-----------------------------------------------------------fx_sis_seldistritos

CREATE OR REPLACE FUNCTION public.fx_sis_seldistritos(
	character varying)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character, ncodigo_provincia integer, cnomprovincia character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
		
	p_cvalbus     ALIAS FOR $1;
	
begin	
	return	query
	select	dis.ncodigo,
		dis.ccodigo,
		dis.cnombre,
		dis.cestado,
		dis.ncodigo_provincia,
		pro.cnombre
	from	sis_t_distritos dis
		inner join sis_t_provincias pro on dis.ncodigo_provincia = pro.ncodigo
	where	(dis.ccodigo ilike '%'|| p_cvalbus ||'%' or dis.cnombre ilike '%'|| p_cvalbus ||'%')

	;

end
$BODY$;

------------------------------------------------------------------------------------------------------

----------------------------------- fx_sis_selperitos

CREATE OR REPLACE FUNCTION public.fx_sis_selperitos(
	character varying)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, capellido_paterno character varying, capellido_materno character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_cvalbus  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	per.ncodigo,
			per.ccodigo,
			per.cnombre,
			per.capellido_paterno,
			per.capellido_materno,
			per.cestado
	from	sis_t_peritos per
	where	per.ccodigo ilike '%'|| p_cvalbus ||'%' or per.cnombre ilike '%'||p_cvalbus||'%'
	order	by ccodigo asc;
end
$BODY$;


--------------------------------------- fx_sis_insperito

CREATE OR REPLACE FUNCTION public.fx_sis_insperito(
	character,
	character varying,
	character varying,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;	
	p_capellido_paterno ALIAS FOR $4;
	p_capellido_materno ALIAS FOR $5;
	p_cestado  ALIAS FOR $3;
	p_nusuario ALIAS FOR $6;
begin
	IF EXISTS (SELECT ccodigo FROM sis_t_peritos WHERE ccodigo = p_ccodigo) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSIF EXISTS (SELECT cnombre FROM sis_t_peritos WHERE cnombre = p_cnombre AND capellido_paterno = p_capellido_paterno AND capellido_materno = p_capellido_materno) THEN
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Duplicado ['|| p_cnombre || ' ' || p_capellido_paterno || ' ' || p_capellido_materno||'].')::CHARACTER VARYING AS cDesTra
		;
	ELSE
		INSERT INTO sis_t_peritos (ccodigo, cnombre, cestado, capellido_paterno, capellido_materno, tcreacion, nusuario_crea)
		VALUES(p_ccodigo, p_cnombre, p_cestado, p_capellido_paterno, p_capellido_materno, now(), p_nusuario  );

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;

-------------------------------------- fx_sis_selperfiles

CREATE OR REPLACE FUNCTION public.fx_sis_selperfiles(
	character varying)
    RETURNS TABLE(ncodigo integer,cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_cnombre  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	per.ncodigo,
			per.cnombre,
			per.cestado
	from	sis_t_perfiles per
	where	per.cnombre ilike '%'||p_cnombre||'%'
	order	by ncodigo asc;
end
$BODY$;


---------------------------------------- fx_sis_selopciones

CREATE OR REPLACE FUNCTION public.fx_sis_selopciones(
	character varying)
    RETURNS TABLE(ncodigo integer, ncodigo_modulo integer, cnombre character varying, caccion character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_cnombre  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	opc.ncodigo,
			opc.ncodigo_modulo,
			opc.cnombre,
			opc.caccion,
			opc.cestado
	from	sis_t_opciones opc
	where	opc.cnombre ilike '%'||p_cnombre||'%'
	order	by ncodigo asc;
end
$BODY$;

------------------------------------------------ fx_sis_selmodulos

CREATE OR REPLACE FUNCTION public.fx_sis_selmodulos(
	character varying)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cicono character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_cvalbus  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	modu.ncodigo,
			modu.ccodigo,
			modu.cnombre,
			modu.cicono,
			modu.cestado
	from	sis_t_modulos modu
	where	modu.ccodigo ilike '%'|| p_cvalbus ||'%' or modu.cnombre ilike '%'||p_cvalbus||'%'
	order	by ccodigo asc;
end
$BODY$;


--------------------------------------------------- fx_sis_selgarantias

CREATE OR REPLACE FUNCTION public.fx_sis_selgarantias(
	character varying)
    RETURNS TABLE(ncodigo integer, ccodigo character, cdescripcion character varying, cpreferida character, ccuenta_contable character, ccodigo_sbs character, cdescripcion_corta character, nperiodo_tasacion integer, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_cvalbus  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	gar.ncodigo,
			gar.ccodigo,
			gar.cdescripcion,
			gar.cpreferida,
			gar.ccuenta_contable,
			gar.ccodigo_sbs,
			gar.cdescripcion_corta,
			gar.nperiodo_tasacion,
			gar.cestado
	from	sis_t_garantias gar
	where	gar.ccodigo ilike '%'|| p_cvalbus ||'%' or gar.cdescripcion ilike '%'||p_cvalbus||'%'
	order	by ccodigo asc;
end
$BODY$;


----------------------------------------------------------- fx_sis_insgarantia

CREATE OR REPLACE FUNCTION public.fx_sis_insgarantia(
	character,
	character varying,
	character,
	character,
	character,
	character,
	integer,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cdescripcion  ALIAS FOR $2;	
	p_cpreferida  ALIAS FOR $3;	
	p_ccuenta_contable  ALIAS FOR $4;
	p_ccodigo_sbs  ALIAS FOR $5;	
	p_cdescripcion_corta  ALIAS FOR $6;	
	p_nperiodo_tasacion ALIAS FOR $7;	
	p_cestado  ALIAS FOR $8;
	p_nusuario ALIAS FOR $9;
begin
	IF EXISTS (SELECT ccodigo FROM sis_t_garantias WHERE ccodigo = p_ccodigo) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSIF EXISTS (SELECT cdescripcion_corta FROM sis_t_garantias WHERE cdescripcion_corta = p_cdescripcion_corta) THEN
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Duplicado ['|| p_cdescripcion_corta ||'].')::CHARACTER VARYING AS cDesTra
		;
	ELSE
		INSERT INTO sis_t_garantias (ccodigo, cdescripcion, p_cpreferida, ccuenta_contable, ccodigo_sbs, cdescripcion_corta, nperiodo_tasacion, cestado, tcreacion, nusuario_crea)
		VALUES(p_ccodigo, p_cdescripcion, p_cpreferida, p_ccuenta_contable, p_ccodigo_sbs, p_cdescripcion_corta, p_nperiodo_tasacion, p_cestado, now(), p_nusuario  );

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;

-------------------------------------------- fx_sis_selferiados

CREATE OR REPLACE FUNCTION public.fx_sis_selferiados(
	character, date, date)
    RETURNS TABLE(ncodigo integer, dfecha date, ctipo character, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ctipo  ALIAS FOR $1;
	p_dfechaIni  ALIAS FOR $2;
	p_dfechaFin  ALIAS FOR $3;
begin
	RETURN	QUERY
	SELECT	fer.ncodigo,
			fer.dfecha,
			fer.ctipo,
			fer.cestado
	from	sis_t_feriados fer
	where	fer.ctipo = p_ctipo or fer.dfecha BETWEEN p_dfechaIni AND p_dfechaFin
	order	by ncodigo asc;
end
$BODY$;


------------------------------- fx_sis_selactividades

CREATE OR REPLACE FUNCTION public.fx_sis_selactividades(
	character varying)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_cvalbus  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	act.ncodigo,
			act.ccodigo,
			act.cnombre,
			act.cestado
	from	sis_t_actividades act
	where	act.ccodigo ilike '%'|| p_cvalbus ||'%' or act.cnombre ilike '%'||p_cvalbus||'%'
	order	by ccodigo asc;
end
$BODY$;


-------------------------------------------- fx_sis_insactividad

CREATE OR REPLACE FUNCTION public.fx_sis_insactividad(
	character,
	character varying,
	character,
	integer)
    RETURNS TABLE(ccodtra character, cdestra character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ccodigo  ALIAS FOR $1;
	p_cnombre  ALIAS FOR $2;	
	p_cestado  ALIAS FOR $3;
	p_nusuario ALIAS FOR $4;
begin
	IF EXISTS (SELECT ccodigo FROM sis_t_actividades WHERE ccodigo = p_ccodigo) THEN
		RETURN	query
		SELECT	'1'::CHARACTER as cCodTra,
			('Registro Duplicado ['|| p_ccodigo ||'].')::CHARACTER varying AS cDesTra
		;
	ELSIF EXISTS (SELECT cnombre FROM sis_t_actividades WHERE cnombre = p_cnombre) THEN
		RETURN	query
		SELECT	'1'::CHARACTER AS cCodTra,
			('Registro Duplicado ['|| p_cnombre ||'].')::CHARACTER VARYING AS cDesTra
		;
	ELSE
		INSERT INTO sis_t_actividades (ccodigo, cnombre, cestado, tcreacion, nusuario_crea)
		VALUES(p_ccodigo, p_cnombre, p_cestado, now(), p_nusuario  );

		RETURN	query
		SELECT	'0'::CHARACTER as cCodTra,
			'Registro Satisfactorio.'::CHARACTER VARYING AS cDesTra
		;
	END IF;
end
$BODY$;

------------------------------- fx_sis_seltpermisos
/*
CREATE OR REPLACE FUNCTION public.fx_sis_seltpermisos(
	integer, integer)
    RETURNS TABLE(ncodigo integer, ncodigo_perfil integer, ncodigo_opcion integer, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo_perfil  ALIAS FOR $1;
	p_ncodigo_opcion  ALIAS FOR $2;
begin
	IF p_ncodigo_opcion = -1 THEN
		RETURN	QUERY
		SELECT	per.ncodigo,
				per.ncodigo_perfil,
				per.ncodigo_opcion,
				per.cestado
		from	sis_p_permisos per
		where	per.ncodigo_perfil = p_ncodigo_perfil
		;
 	ELSE
		RETURN	QUERY
		SELECT	per.ncodigo,
				per.ncodigo_perfil,
				per.ncodigo_opcion,
				per.cestado
		from	sis_p_permisos per
		where	per.ncodigo_opcion = p_ncodigo_opcion
		;
 	END IF;
end
$BODY$;
*/
------------------------------- fx_sis_seltperfiles

CREATE OR REPLACE FUNCTION public.fx_sis_seltperfiles(
	integer, integer)
    RETURNS TABLE(ncodigo integer, ncodigo_usuario integer, ncodigo_perfil integer, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo_perfil  ALIAS FOR $1;
	p_ncodigo_opcion  ALIAS FOR $2;
begin
	IF p_ncodigo_perfil = -1 THEN
		RETURN	QUERY
		SELECT	per.ncodigo,
				per.ncodigo_usuario,
				per.ncodigo_perfil,
				per.cestado
		from	sis_p_perfiles per
		where	per.ncodigo_usuario = p_ncodigo_usuario
		;
 	ELSE
		RETURN	QUERY
		SELECT	per.ncodigo,
				per.ncodigo_usuario,
				per.ncodigo_perfil,
				per.cestado
		from	sis_p_perfiles per
		where	per.ncodigo_perfil = p_ncodigo_perfil
		;
 	END IF;
end
$BODY$;


