

CREATE OR REPLACE FUNCTION public.fx_sis_selpais(
	integer)
    RETURNS TABLE(ncodigo integer, ccodigo character varying, cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	pais.ncodigo,
			pais.ccodigo,
			pais.cnombre,
			pais.cestado
	from	sis_t_paises pais
	WHERE pais.ncodigo = p_ncodigo
	;
end
$BODY$;

---------------------------- fx_sis_seldepartamentos

CREATE OR REPLACE FUNCTION public.fx_sis_seldepartamento(
	integer)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character, ncodigo_pais integer, cnompais character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
		
	p_ncodigo ALIAS FOR $1;
	
begin	
	return	query
	select	dep.ncodigo,
		dep.ccodigo,
		dep.cnombre,
		dep.cestado,
		dep.ncodigo_pais,
		pais.cnombre
	from	sis_t_departamentos dep
		inner join sis_t_paises pais on dep.ncodigo_pais = pais.ncodigo
	where	dep.ncodigo = p_ncodigo

	;
end
$BODY$;



---------------------------------------------fx_sis_selprovincias

CREATE OR REPLACE FUNCTION public.fx_sis_selprovincia(
	integer)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character, ncodigo_departamento integer, cnomdep character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
		
	p_ncodigo ALIAS FOR $1;
	
begin	
	return	query
	select	pro.ncodigo,
		pro.ccodigo,
		pro.cnombre,
		pro.cestado,
		pro.ncodigo_departamento,
		dep.cnombre
	from	sis_t_provincias pro
		inner join sis_t_departamentos dep on pro.ncodigo_departamento = dep.ncodigo
	where	pro.ncodigo = p_ncodigo

	;

end
$BODY$;




-----------------------------------------------------------fx_sis_seldistritos

CREATE OR REPLACE FUNCTION public.fx_sis_seldistrito(
	integer)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character, ncodigo_provincia integer, cnomprovincia character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
		
	p_ncodigo ALIAS FOR $1;
	
begin	
	return	query
	select	dis.ncodigo,
		dis.ccodigo,
		dis.cnombre,
		dis.cestado,
		dis.ncodigo_provincia,
		pro.cnombre
	from	sis_t_distritos dis
		inner join sis_t_provincias pro on dis.ncodigo_provincia = pro.ncodigo
	where	dis.ncodigo = p_ncodigo

	;

end
$BODY$;

------------------------------------------------------------------------------------------------------

----------------------------------- fx_sis_selperito

CREATE OR REPLACE FUNCTION public.fx_sis_selperito(
	integer)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, capellido_paterno character varying, capellido_materno character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	per.ncodigo,
			per.ccodigo,
			per.cnombre,
			per.capellido_paterno,
			per.capellido_materno,
			per.cestado
	from	sis_t_peritos per
	where	per.ncodigo = p_ncodigo
	;
end
$BODY$;




-------------------------------------- 

CREATE OR REPLACE FUNCTION public.fx_sis_selperfil(
	integer)
    RETURNS TABLE(ncodigo integer,cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	per.ncodigo,
			per.cnombre,
			per.cestado
	from	sis_t_perfiles per
	where	per.ncodigo = p_ncodigo
	;
end
$BODY$;


---------------------------------------- 

CREATE OR REPLACE FUNCTION public.fx_sis_selopcion(
	integer)
    RETURNS TABLE(ncodigo integer, ncodigo_modulo integer, cnombre character varying, caccion character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	opc.ncodigo,
			opc.ncodigo_modulo,
			opc.cnombre,
			opc.caccion,
			opc.cestado
	from	sis_t_opciones opc
	where	opc.ncodigo = p_ncodigo
	;
end
$BODY$;

------------------------------------------------ 

CREATE OR REPLACE FUNCTION public.fx_sis_selmodulo(
	integer)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cicono character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	modu.ncodigo,
			modu.ccodigo,
			modu.cnombre,
			modu.cicono,
			modu.cestado
	from	sis_t_modulos modu
	where	modu.ncodigo = p_ncodigo
	;
end
$BODY$;


---------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selgarantia(
	integer)
    RETURNS TABLE(ncodigo integer, ccodigo character, cdescripcion character varying, cpreferida character, ccuenta_contable character, ccodigo_sbs character, cdescripcion_corta character, nperiodo_tasacion integer, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	gar.ncodigo,
			gar.ccodigo,
			gar.cdescripcion,
			gar.cpreferida,
			gar.ccuenta_contable,
			gar.ccodigo_sbs,
			gar.cdescripcion_corta,
			gar.nperiodo_tasacion,
			gar.cestado
	from	sis_t_garantias gar
	where	gar.ncodigo = p_ncodigo
	;
end
$BODY$;


--------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selferiado(
	integer)
    RETURNS TABLE(ncodigo integer, dfecha date, ctipo character, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE

	p_ncodigo  ALIAS FOR $1;

begin
	RETURN	QUERY
	SELECT	fer.ncodigo,
			fer.dfecha,
			fer.ctipo,
			fer.cestado
	from	sis_t_feriados fer
	where	fer.ncodigo = p_ncodigo
	;
end
$BODY$;


------------------------------- 

CREATE OR REPLACE FUNCTION public.fx_sis_selactividad(
	character varying)
    RETURNS TABLE(ncodigo integer, ccodigo character, cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE

	p_ncodigo  ALIAS FOR $1;

begin
	RETURN	QUERY
	SELECT	act.ncodigo,
			act.ccodigo,
			act.cnombre,
			act.cestado
	from	sis_t_actividades act
	where	act.ncodigo = p_ncodigo
	;
end
$BODY$;


-------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selpermiso(
	integer)
    RETURNS TABLE(ncodigo integer, ncodigo_perfil integer, ncodigo_opcion integer, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE

	p_ncodigo  ALIAS FOR $1;

begin
	RETURN	QUERY
	SELECT	per.ncodigo,
			per.ncodigo_perfil,
			per.ncodigo_opcion,
			per.cestado
	from	sis_p_permisos per
	where	per.ncodigo = p_ncodigo
	;
end
$BODY$;

------------------------------- 

CREATE OR REPLACE FUNCTION public.fx_sis_selperfil(
	integer)
    RETURNS TABLE(ncodigo integer, ncodigo_usuario integer, ncodigo_perfil integer, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE

	p_ncodigo  ALIAS FOR $1;

begin
	RETURN	QUERY
	SELECT	per.ncodigo,
			per.ncodigo_usuario,
			per.ncodigo_perfil,
			per.cestado
	from	sis_p_perfiles per
	where	per.ncodigo = p_ncodigo
	;
end
$BODY$;


-------------------------------------

CREATE OR REPLACE FUNCTION public.fx_tes_selconcepto(
	integer)
    RETURNS TABLE(ncodigo integer, cnombre character varying, cctacnt character varying, cestado character, lafecaj boolean, ncodofi integer, cnomofi character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	RETURN	query
	select	con.ncodigo,
		con.cnombre,
		con.cctacnt,
		con.cestado,
		con.lafecaj,
		con.ncodofi,
		ofi.cnombre
	from	tes_t_conceptos con
		inner join sis_t_oficinas ofi on con.ncodofi = ofi.ncodigo
	where	con.ncodigo = p_ncodigo

	;
	
end
$BODY$;


--------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_selvariable(
	integer)
    RETURNS TABLE(ncodigo integer, ccodigo character varying, cnombre character varying, cvalor character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	RETURN	QUERY
	select	var.ncodigo,
		var.ccodigo,
		var.cnombre,
		var.cvalor,
		var.cestado
	from	sis_t_variables var
	WHERE var.ncodigo = p_ncodigo
	;
end
$BODY$;

-----------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selterminal(
	integer)
    RETURNS TABLE(ncodigo integer, cip character varying, ccodigo character, cestado character, ncodigo_oficina integer, cnomofi character varying) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	ter.ncodigo,
		ter.cip,
		ter.ccodigo,
		ter.cestado,
		ter.ncodigo_oficina,
		ofi.cnombre 
	from	sis_t_terminales ter
		inner join sis_t_oficinas ofi on ter.ncodigo_oficina = ofi.ncodigo
	WHERE ter.ncodigo = p_ncodigo
	;
end
$BODY$;

-----------------------------------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION public.fx_sis_selprofesion(
	integer)
    RETURNS TABLE(ncodigo integer, ccodigo character varying, cnombre character varying, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo  ALIAS FOR $1;
begin
	RETURN	QUERY
	SELECT	prof.ncodigo,
			prof.ccodigo,
			prof.cnombre,
			prof.cestado
	from	sis_t_profesiones prof
	WHERE prof.ncodigo = p_ncodigo
	;
end
$BODY$;

------------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.fx_sis_selpperfil(
	integer, integer)
    RETURNS TABLE(ncodigo integer, ncodigo_usuario integer, ncodigo_perfil integer, cestado character) 
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    ROWS 1000
AS $BODY$
DECLARE
	p_ncodigo_usuario ALIAS FOR $1;
	p_ncodigo_perfil ALIAS FOR $2;
begin
	IF p_ncodigo_usuario <> -1 THEN
		RETURN	QUERY
		SELECT	
				A.ncodigo,
				A.ncodigo_usuario,
				A.ncodigo_perfil,
				A.cestado
		from	sis_p_perfiles as A
		where	A.ncodigo_usuario = p_ncodigo_usuario;
	ELSE
		RETURN	QUERY
		SELECT	
				A.ncodigo,
				A.ncodigo_usuario,
				A.ncodigo_perfil,
				A.cestado
		from	sis_p_perfiles as A
		where	A.ncodigo_perfil = p_ncodigo_perfil;
	END IF;
end
$BODY$;

