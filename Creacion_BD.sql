
create table sis_t_distritos(
	ncodigo serial primary key,
	cnombre varchar
);

--------------------------------------------------------------------------
create table sis_t_direcciones(
ncodigo serial primary key,
cdireccion varchar,
ndistrito int,
foreign key (ndistrito) references sis_t_distritos(ncodigo),
creferencia varchar);

----------------------------------------
Create TABLE sis_m_usuarios(
ncodigo serial primary key,
cnombres varchar,
capellido_p varchar,
capellido_m varchar,
nrdoc int default null,
ctipdoc char default null,
ccorreo varchar,
cclave varchar,
ncel int,
ntelf int default null,
cestado character,
tfecha_registro DATE,
ncodrecup varchar
);
---------------------------------------------------
create table sis_p_direccion_cliente(
ncodigo serial primary key,
ncoddir int,
foreign key(ncoddir) references sis_t_direcciones(ncodigo),
ncodusu int,
foreign key(ncodusu) references sis_m_usuarios(ncodigo));

---------------------------------------------------

create table sis_t_perfiles(
ncodigo serial primary key,
cnombre varchar,
cestado char);
-----------------------------------------------------
create table sis_p_perfiles(
ncodigo serial primary key,
ncodigo_perfil int,
foreign key(ncodigo_perfil) references sis_t_perfiles(ncodigo),
cestado char,
ncodigo_u int,
foreign key(ncodigo_u) references sis_m_usuarios(ncodigo));
------------------------------------------------------------------
create table sis_t_modulos(
ncodigo serial primary key,
ccodigo varchar,
cnombre varchar,
cestado char	
);

------------------------------------------------------------
create table sis_t_opciones(
ncodigo serial primary key,
ncodigo_modulo int,
foreign key(ncodigo_modulo) references sis_t_modulos(ncodigo),
cnombre varchar,
caccion varchar,
cestado char
);
-------------------------------------------------

create table sis_t_log(
ncodigo serial primary key,
ncodigo_usuario int,
foreign key(ncodigo_usuario) references sis_m_usuarios(ncodigo),
ncodigo_opcion int,
foreign key(ncodigo_opcion) references sis_t_opciones(ncodigo),
cglosa varchar);
-------------------------------------------------------------
CREATE TABLE public.sis_t_categoria
(
    ncodigo serial NOT NULL,
    cnombre character varying COLLATE pg_catalog."default",
    CONSTRAINT sis_t_categoria_pkey PRIMARY KEY (ncodigo)
);
--------------------------
create table sis_m_productos(
ncodigo serial primary key,
cnombre varchar,
cdescripcion varchar,
ncategoria int,
foreign key(ncategoria) references sis_t_categoria(ncodigo),
nprecio float,
nstock int default 0,
img text,
cestado character
);
--------------------------------------------------------------
create table sis_p_permisos(
ncodigo serial primary key,
ncodigo_perfil int,
foreign key(ncodigo_perfil) references sis_t_perfiles(ncodigo),
ncodigo_opcion int,
foreign key(ncodigo_opcion) references sis_t_opciones(ncodigo),
cestado char);
---------------------------------------------------------------
create table sis_m_pedidos(
ncodigo serial primary key,
ncliente int,
foreign key(ncliente) references sis_m_usuarios(ncodigo),
ctippag character,
ntotal float,
ndireccion int,
foreign key(ndireccion) references sis_t_direcciones(ncodigo),
cestado character,
tfecha_creacion DATE,
thora TIME
);



------------------------------------------------------------

create table sis_d_pedidos(
ncodigo serial primary key,
npedido int,
foreign key(npedido) references sis_m_pedidos(ncodigo),	
nproducto int,
foreign key(nproducto) references sis_m_productos(ncodigo),
ncantidad int,
npreciounitario float,
ntotal float,
cestado character,
tfecha_creacion DATE
);



-----------------------------------------------------------

create table sis_t_documento_pedido(
	ncodigo serial primary key,
	npedido int,
	foreign key(npedido) references sis_m_pedidos(ncodigo),
	ctipodocumento character,
	cestado character,
	tfecha_creacion DATE,
	nusuario_crea int
);




---------------------------------------------------------------

create table sis_t_movimientos(
	ncodigo serial primary key,
	nproducto int,
	foreign key(nproducto) references sis_m_productos(ncodigo),
	ncantidad int,
	cestado character,
    nusuario_crea int
);

-------------------------------------------------------------------
CREATE TABLE public.sis_t_estadospedido
(
    ncodigo serial NOT NULL,
    cnombre character varying COLLATE pg_catalog."default",
    cestado character(1) COLLATE pg_catalog."default",
    CONSTRAINT sis_t_estadospedido_pkey PRIMARY KEY (ncodigo)
);
-----------------------------------------------------------------
CREATE TABLE public.sis_p_estado_pedido
(
    ncodigo serial NOT NULL,
    npedido integer,
    nestado integer,
    tfecha_creacion date,
	thora TIME,
    nusuario_crea integer,
    CONSTRAINT sis_p_estado_pedido_pkey PRIMARY KEY (ncodigo),
    CONSTRAINT sis_p_estado_pedido_nestado_fkey FOREIGN KEY (nestado)
        REFERENCES public.sis_t_estadospedido (ncodigo) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT sis_p_estado_pedido_npedido_fkey FOREIGN KEY (npedido)
        REFERENCES public.sis_m_pedidos (ncodigo) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);
------------------------------------------------------------------------------







