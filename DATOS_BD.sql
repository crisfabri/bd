--CATEGORIAS
INSERT INTO SIS_T_CATEGORIA(cnombre) VALUES('PIQUEOS');
INSERT INTO SIS_T_CATEGORIA(cnombre) VALUES('CORTES CRIOLLOS INDIVIDUALES');
INSERT INTO SIS_T_CATEGORIA(cnombre) VALUES('CORTES ENTEROS NACIONALES PREMIUM');
INSERT INTO SIS_T_CATEGORIA(cnombre) VALUES('CORTES PORCIONADOS CORTADOS PREMIUM');
INSERT INTO SIS_T_CATEGORIA(cnombre) VALUES('CORTES IMPORTADOS ENTEROS');
INSERT INTO SIS_T_CATEGORIA(cnombre) VALUES('GUARNICIONES');
INSERT INTO SIS_T_CATEGORIA(cnombre) VALUES('ESPECIALES(Con papas)');
INSERT INTO SIS_T_CATEGORIA(cnombre) VALUES('BEBIDAS');
INSERT INTO SIS_T_CATEGORIA(cnombre) VALUES('DELIVERY(Con papas y ensalada)');
------------------------------------------------------------------
--PERFILES
INSERT INTO SIS_T_PERFILES(cnombre,cestado) values ('CLIENTE','A');
INSERT INTO SIS_T_PERFILES(cnombre,cestado) values ('ADMINISTRADOR','A');
---------------------------------------------------------------
INSERT INTO SIS_T_ESTADOSPEDIDO(cnombre,cestado) values ('SOLICITADO','A');
INSERT INTO SIS_T_ESTADOSPEDIDO(cnombre,cestado) values ('TRANSPORTE','A');
INSERT INTO SIS_T_ESTADOSPEDIDO(cnombre,cestado) values ('ENTREGADO','A');
----------------------------------------------------------------------
INSERT INTO SIS_T_MODULOS(ccodigo,cnombre,cestado) values(1,'LOGIN','A');
INSERT INTO SIS_T_OPCIONES(ncodigo_modulo,cnombre,caccion,cestado) values (1,'LOGIN','','A');
----------------------------------------------------------------------------------
INSERT INTO SIS_T_DIRECCIONES(cdireccion) VALUES ('En local');
----------------------------------------------------------------------------

